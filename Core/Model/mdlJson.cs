﻿/* documentation
 * 001 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{
    public class mdlJson
    {
        [DataMember]
        public List<mdlCustomer> CustomerList { get; set; }

        [DataMember]
        public List<mdlCustomerType> CustomerTypeList { get; set; }

        
        [DataMember]
        public List<mdlDeliveryOrder> DeliveryOrderList { get; set; }

        [DataMember]
        public List<mdlDeliveryOrderDetail> DeliveryOrderDetailList { get; set; }

        [DataMember]
        public List<mdlProduct> ProductList { get; set; }

        [DataMember]
        public List<mdlReturOrder> ReturOrderList { get; set; }  

        [DataMember]
        public List<mdlReturOrderDetail> ReturOrderDetailList { get; set; } 

        [DataMember]
        public List<mdlCallPlan> CallPlanList { get; set; }
        [DataMember]
        public List<mdlCallPlanDetail> CallPlanDetailList { get; set; } 

        [DataMember]
        public List<mdlCost> CostList { get; set; } 
        [DataMember]
        public List<mdlDailyMsg> DailyMessageList { get; set; } 

        [DataMember]
        public List<mdlReason> ReasonList { get; set; } 
        [DataMember]
        public List<mdlProductUOM> ProductUOMList { get; set; }

        //christopher
        [DataMember]
        public List<mdlMobileConfig> MobileConfigList { get; set; } //009
        //christopher
        [DataMember]
        public List<mdlWarehouse> WarehouseList { get; set; }

        //fernandes
        [DataMember]
        public string Result { get; set; }

        [DataMember]
        public List<mdlDeliveryOrderDetail> UnvalidDOProductList { get; set; }

        [DataMember]
        public List<mdlDBVisit> Visit { get; set; }

        [DataMember]
        public List<mdlDBVisitDetail2> VisitDetailList { get; set; }

        [DataMember]
        public List<mdlDailyCost> DailyCostList { get; set; }
    }

    public class mdlJsonList
    {
        public List<mdlJson> mdlJson { get; set; }
    }
}
