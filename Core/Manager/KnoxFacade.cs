﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Core.Manager
{
    public class KnoxFacade : Base.Manager
    {
        public static Model.mdlKnox LoadKnoxKey(Model.mdlKnoxParam param)
        {
            var mdlKnox = new Model.mdlKnox();

            List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@Password", SqlDbType = SqlDbType.NVarChar, Value = param.Password },
                };

            try
            {
                DataTable dtKnoxKey = Manager.DataFacade.DTSQLCommand(@"SELECT KnoxKey
                                  FROM KnoxLicense
                                  WHERE Password = @Password", sp); //006

                if (dtKnoxKey.Rows.Count == 0)
                {
                    mdlKnox.KnoxKey = "";
                    mdlKnox.Status = "0";
                    mdlKnox.Message = "Password salah";
                }
                else
                {
                    foreach (DataRow row in dtKnoxKey.Rows)
                    {
                        mdlKnox.KnoxKey = row["KnoxKey"].ToString();
                        mdlKnox.Status = "1";
                        mdlKnox.Message = "Berhasil";
                    }
                }
            }
            catch
            {
                   mdlKnox.KnoxKey = "";
                   mdlKnox.Status = "00";
                   mdlKnox.Message = "Error SQL";
            }

            return mdlKnox;
        }
    }
}
