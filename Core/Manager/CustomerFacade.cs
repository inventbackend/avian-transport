﻿/* documentation
 *001 - 14 Okt'16 - fernandes
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Core.Manager
{
    public class CustomerFacade : Base.Manager
    {
        public static List<Model.mdlCustomer> LoadCustomer(Model.mdlParam json)
        {

            var mdlCustomerList = new List<Model.mdlCustomer>();

            List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@Date", SqlDbType = SqlDbType.DateTime, Value = Convert.ToDateTime(json.Date).Date },
                    new SqlParameter() {ParameterName = "@EmployeeID", SqlDbType = SqlDbType.NVarChar, Value = json.EmployeeID },
                    new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = json.BranchID }
                };

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(@"SELECT a.* FROM Customer a 
				                                                    INNER JOIN
				                                                    (SELECT CustomerID FROM CallPlanDetail WHERE CallPlanID IN (SELECT CallPlanID FROM CallPlan WHERE EmployeeID = @EmployeeID and BranchID = @BranchID and Date >= @Date and Date < DATEADD(day,1,@Date) and IsFinish=0 )) b 
				                                                    ON a.CustomerID = b.CustomerID;", sp);

            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                var mdlCustomer = new Model.mdlCustomer();
                mdlCustomer.CustomerID = drCustomer["CustomerID"].ToString();
                mdlCustomer.CustomerName = drCustomer["CustomerName"].ToString();
                mdlCustomer.CustomerAddress = drCustomer["CustomerAddress"].ToString();
                mdlCustomer.CustomerTypeID = drCustomer["CustomerTypeID"].ToString();
                mdlCustomer.Latitude = drCustomer["Latitude"].ToString();
                mdlCustomer.Longitude = drCustomer["Longitude"].ToString();
                mdlCustomer.Phone = drCustomer["Phone"].ToString();
                mdlCustomer.PIC = drCustomer["PIC"].ToString();
                mdlCustomer.BranchID = drCustomer["BranchID"].ToString();
                mdlCustomer.Radius = drCustomer["Radius"].ToString();
                mdlCustomerList.Add(mdlCustomer);
            }
            return mdlCustomerList;
        }

        public static Model.mdlCustomer GetSearchCustomerById(string customerId)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = customerId }
            };

            var mdlCust = new Model.mdlCustomer();

            //var mdlCustList = new List<Model.mdlCustomer>();

            DataTable dtCust = Manager.DataFacade.DTSQLCommand(@"SELECT TOP 1 CustomerID, 
			                                                                        Latitude,
                                                                                    Longitude,
                                                                                    Radius
                                                                                    from Customer
				                                                            Where CustomerID=@CustomerID", sp);

            foreach (DataRow row in dtCust.Rows)
            {
                
                mdlCust.CustomerID = row["CustomerID"].ToString();
                mdlCust.Latitude = row["Latitude"].ToString();
                mdlCust.Longitude = row["Longitude"].ToString();
                mdlCust.Radius = row["Radius"].ToString();
            }

            return mdlCust;
        }

        public static List<Model.Customer> GetSearch(string keyword,string branchid)
        {
            var customer = DataContext.Customers.Where(fld => (fld.CustomerID.Contains(keyword) || fld.CustomerName.Contains(keyword) || fld.CustomerAddress.Contains(keyword)) && fld.BranchID.Contains(branchid)).OrderBy(fld => fld.CustomerName).ToList();
            return customer;
        }

        public static List<Model.mdlCustomer> GetSearchCustomerByBranch(string keyword, string branchid)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
            {
            };

            var mdlCustList = new List<Model.mdlCustomer>();

            DataTable dtCust = Manager.DataFacade.DTSQLCommand(@"SELECT   CustomerID, 
			                                                                        CustomerName,
                                                                                    CustomerAddress, BranchID
                                                                                    from Customer
				                                                            Where BranchID IN (" + branchid + ") and (CustomerID LIKE '%" + keyword + "%' or CustomerName LIKE '%"+keyword+"%')", sp);

            foreach (DataRow row in dtCust.Rows)
            {
                var mdlCust = new Model.mdlCustomer();
                mdlCust.CustomerID = row["CustomerID"].ToString();
                mdlCust.CustomerName = row["CustomerName"].ToString();
                mdlCust.CustomerAddress = row["CustomerAddress"].ToString();
                mdlCust.BranchID = row["BranchID"].ToString();

                mdlCustList.Add(mdlCust);
            }
            return mdlCustList;
        }

        //untuk SettingKoordinat.aspx
        public static List<Model.Customer> GetSearch2(string ddlcabang, string keyword)
        {
            var customer = DataContext.Customers.Where(fld => fld.BranchID.Equals(ddlcabang)).Where(fld => fld.CustomerID.ToLower().Contains(keyword.ToLower()) || fld.CustomerName.ToLower().Contains(keyword.ToLower()) || fld.CustomerAddress.ToLower().Contains(keyword.ToLower())).OrderBy(fld => fld.CustomerName).ToList();
            return customer;
        }

        public static Model.Customer GetCustomerDetail(string keyword)
        {
            var customer = DataContext.Customers.FirstOrDefault(fld => fld.CustomerID.Contains(keyword));
            return customer;
        }

        //001
        public static void UpdateLongLatCustomer(string lCustomerID, string lLongitude, string lLatitude, decimal lRadius)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = lCustomerID },
                    new SqlParameter() {ParameterName = "@Longitude", SqlDbType = SqlDbType.NVarChar, Value = lLongitude },
                    new SqlParameter() {ParameterName = "@Latitude", SqlDbType = SqlDbType.NVarChar, Value = lLatitude },
                    new SqlParameter() {ParameterName = "@Radius", SqlDbType = SqlDbType.Decimal, Value = lRadius }
                };

            string query = @"UPDATE Customer SET 
                                            Longitude = @Longitude, 
                                            Latitude = @Latitude,
                                            Radius = @Radius
                                            WHERE CustomerID = @CustomerID";

            Manager.DataFacade.DTSQLVoidCommand(query, sp);

            return;
        }

        public static List<Model.mdlCustomer> GetCustomerCoordinate(string lBranchID)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
            {
               new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
            };

            var mdlCustomerList = new List<Model.mdlCustomer>();

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(@"SELECT CustomerID, CustomerName, CustomerAddress, Phone, PIC, Latitude, Longitude
                                                                        FROM Customer
                                                                        WHERE BranchID=@BranchID AND Latitude <> '' AND Latitude <> '0' AND Latitude IS NOT NULL
                                                                                                 AND Longitude <> '' AND Longitude <> '0' AND Longitude IS NOT NULL", sp);

            foreach (DataRow row in dtCustomer.Rows)
            {
                var mdlCustomer = new Model.mdlCustomer();
                mdlCustomer.CustomerID = row["CustomerID"].ToString();
                mdlCustomer.CustomerName = row["CustomerName"].ToString();
                mdlCustomer.CustomerAddress = row["CustomerAddress"].ToString();
                mdlCustomer.Phone = row["Phone"].ToString();
                mdlCustomer.PIC = row["PIC"].ToString();
                mdlCustomer.Latitude = row["Latitude"].ToString();
                mdlCustomer.Longitude = row["Longitude"].ToString();

                mdlCustomerList.Add(mdlCustomer);
            }
            return mdlCustomerList;
        }

        public static List<Model.mdlCustomer2> LoadCustomernWarehouse(string customerID)
        {

            var mdlCustomerList = new List<Model.mdlCustomer2>();

            List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = customerID},
                };

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(@"select a.customerid,a.customername,a.customeraddress from customer a where a.customerid=@CustomerID
                                                                    union all
                                                                    select b.warehouseid,b.warehousename,b.warehouseaddress from Warehouse b where b.customerid=@CustomerID", sp);

            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                var mdlCustomer = new Model.mdlCustomer2();
                mdlCustomer.CustomerID = drCustomer["CustomerID"].ToString();
                //mdlCustomer.CustomerName = drCustomer["CustomerName"].ToString();
                //mdlCustomer.CustomerAddress = drCustomer["CustomerAddress"].ToString();
                mdlCustomer.CustomerName = drCustomer["CustomerName"] + " - " + drCustomer["CustomerID"].ToString();

                mdlCustomerList.Add(mdlCustomer);
            }
            return mdlCustomerList;
        }

        public static List<Model.mdlCustomer2> LoadCustomernWarehouseAddr(string ID, string customerid)
        {

            var mdlCustomerList = new List<Model.mdlCustomer2>();

            List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@ID", SqlDbType = SqlDbType.NVarChar, Value = ID},
                    new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = customerid},
                };

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(@"select a.CustomerAddress, 'customer' as CustomerType from customer a where a.customerid=@ID
                                                                    union all
                                                                    select b.WarehouseAddress, 'warehouse' as CustomerType from Warehouse b where b.warehouseid=@ID and b.CustomerID=@CustomerID", sp);

            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                var mdlCustomer = new Model.mdlCustomer2();
                mdlCustomer.CustomerAddress = drCustomer["CustomerAddress"].ToString();
                mdlCustomer.CustomerType = drCustomer["CustomerType"].ToString();

                mdlCustomerList.Add(mdlCustomer);
            }
            return mdlCustomerList;
        }

        //FERNANDES
        public static Model.mdlCustomerLicense LoadCustomerLicenseKey(string customerid)
        {

            var mdlCustomerLicense = new Model.mdlCustomerLicense();

            List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = customerid},
                };

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(@"SELECT LicenseKey FROM CustomerLicense WHERE CustomerID=@CustomerID", sp);

            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                mdlCustomerLicense.LicenseKey = drCustomer["LicenseKey"].ToString();
            }

            return mdlCustomerLicense;
        }

        //FERNANDES
        public static List<Model.mdlCustomer> GetCustomerorWarehouseCoordinate(List<Model.mdlVisitTracking> lParamlist)
        {
            var mdlCustomerList = new List<Model.mdlCustomer>();

            foreach (var lParam in lParamlist)
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                   new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.CustomerID},
                   new SqlParameter() {ParameterName = "@WarehouseID", SqlDbType = SqlDbType.NVarChar, Value = lParam.WarehouseID}
                };

                if (lParam.CustomerID == lParam.WarehouseID)
                {
                    DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(@"SELECT CustomerID, CustomerName, Latitude, Longitude,CustomerAddress
                                                                            FROM Customer
                                                                            WHERE CustomerID=@CustomerID AND Latitude <> '' AND Latitude <> '0' AND Latitude IS NOT NULL
                                                                                                     AND Longitude <> '' AND Longitude <> '0' AND Longitude IS NOT NULL", sp);

                    foreach (DataRow row in dtCustomer.Rows)
                    {
                        var mdlCustomer = new Model.mdlCustomer();
                        mdlCustomer.CustomerID = row["CustomerID"].ToString();
                        mdlCustomer.CustomerName = row["CustomerName"].ToString();
                        mdlCustomer.CustomerAddress = row["CustomerAddress"].ToString();
                        mdlCustomer.Latitude = row["Latitude"].ToString();
                        mdlCustomer.Longitude = row["Longitude"].ToString();

                        mdlCustomerList.Add(mdlCustomer);
                    }
                }
                else
                {
                    DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(@"SELECT WarehouseID, WarehouseName, Latitude, Longitude, WarehouseAddress
                                                                            FROM Warehouse
                                                                            WHERE CustomerID=@CustomerID AND WarehouseID=@WarehouseID AND Latitude <> '' AND Latitude <> '0' AND Latitude IS NOT NULL
                                                                                                     AND Longitude <> '' AND Longitude <> '0' AND Longitude IS NOT NULL", sp);

                    foreach (DataRow row in dtCustomer.Rows)
                    {
                        var mdlCustomer = new Model.mdlCustomer();
                        mdlCustomer.CustomerID = row["WarehouseID"].ToString();
                        mdlCustomer.CustomerName = row["WarehouseName"].ToString();
                        mdlCustomer.CustomerAddress = row["WarehouseAddress"].ToString();
                        mdlCustomer.Latitude = row["Latitude"].ToString();
                        mdlCustomer.Longitude = row["Longitude"].ToString();

                        mdlCustomerList.Add(mdlCustomer);
                    }
                }
                
            }

            return mdlCustomerList;
       }

    }
}
