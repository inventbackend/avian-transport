﻿/* documentation
 * 001 nanda - 18 jul 2016
 * 002 nanda - 20 jul 2016
 * 003 nanda - 21 jul 2016
 * 004 fernandes - 21 jul 2016
 * 005 nanda - 25 jul 2016
 * 006 fernandes - 28 jul 2016
 * 007 nanda - 12 agt 2016
 * 008 fernandes - 15 Ags 2016
 * 009 nanda - 18 Ags 2016
 * 010 fernandes - 26 Ags 2016
 * 011 nanda -  29 Ags 2016
 * 012 fernandes - 01 sept 2016
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Core.Manager;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//Update christopher
using System.Device.Location;
using System.Globalization;


namespace TransportService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DeliveryOrderService" in code, svc and config file together.
    public class DeliveryOrderService : IDeliveryOrderService
    {
        //001
        public Core.Model.mdlJsonList GetJson(Core.Model.mdlParam json)
        {
            var resultJson = new Core.Model.mdlJsonList();
            resultJson = JsonFacade.LoadJson(json);
            string result = "";
            if (resultJson.mdlJson.FirstOrDefault().CallPlanList.Count > 0)
            {
                result = "Success";
            }
            else
            {
                result = "Failed";
            }
            var strJson = Core.Services.RestPublisher.Serialize(resultJson);
            var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
            string sizeKB = size.ToString() + " KB";


            Core.Manager.LogFacade.InsertLog("DownloadJson", json.EmployeeID, result, json.BranchID, json.deviceID, "", sizeKB);

            return resultJson;
        }
        //001

        public Core.Model.mdlResult CheckConnection()
        {
            var result = new Core.Model.mdlResult();
            result.Result = "1";

            return result;
        }

        //--010
        public Core.Model.mdlResultList InsertReturOrder(List<Core.Model.mdlReturOrderParam> lParamlist)
        {
            string serlJson = JsonConvert.SerializeObject(lParamlist);
            string lEmployeeID = lParamlist.FirstOrDefault().EmployeeID;



            var resultInsertRetur = ReturFacade.InsertReturOrder(lParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultInsertRetur);



            Core.Manager.LogFacade.InsertLog("DownloadJson", lEmployeeID, JsonResultlist, lParamlist.FirstOrDefault().BranchID, "", "", "");

            return resultInsertRetur;
        }

        public Core.Model.mdlResultList InsertReturOrderDetail(List<Core.Model.mdlReturOrderDetailParam> lParamlist)
        {
            string serlJson = JsonConvert.SerializeObject(lParamlist);
            string lEmployeeID = lParamlist.FirstOrDefault().EmployeeID;



            var resultInsertReturDetail = ReturFacade.InsertReturOrderDetail(lParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultInsertReturDetail);

            Core.Manager.LogFacade.InsertLog("DownloadJson", lEmployeeID, JsonResultlist, "", "", "", "");

            return resultInsertReturDetail;
        }
        //010--

        //002, --006
        public Core.Model.mdlResultList UpdateRetur(List<Core.Model.mdlReturOrderParam> lParamlist)
        {
            string serlJson = JsonConvert.SerializeObject(lParamlist);
            string lEmployeeID = lParamlist.FirstOrDefault().EmployeeID;



            var resultRetur = ReturFacade.UpdateReturOrder(lParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultRetur);
            //--009
            //JsonResultlist = JsonResultlist.Substring(0, 500);
            //009--
            Core.Manager.LogFacade.InsertLog("UpdateReturOrder", lEmployeeID, JsonResultlist, lParamlist.FirstOrDefault().BranchID, "", "", "");

            return resultRetur;
        }

        public Core.Model.mdlResultList UpdateReturDetail(List<Core.Model.mdlReturOrderDetailParam> lParamlist)
        {
            string serlJson = JsonConvert.SerializeObject(lParamlist);
            string lEmployeeID = lParamlist.FirstOrDefault().EmployeeID;


            var resultReturDetail = ReturFacade.UpdateReturOrderDetail(lParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultReturDetail);
            //--009
            //JsonResultlist = JsonResultlist.Substring(0, 500);
            //009--
            Core.Manager.LogFacade.InsertLog("UpdateReturOrderDetail", lEmployeeID, JsonResultlist, "", "", "", "");

            return resultReturDetail;
        }
        //002

        //003
        public Core.Model.mdlResultList InsertCustomerImage(List<Core.Model.mdlCustomerImageParam> lParamlist)
        {
            string serlJson = JsonConvert.SerializeObject(lParamlist);
            string lEmployeeID = lParamlist.FirstOrDefault().EmployeeID;



            var resultCustomerImage = CustomerImageFacade.InsertCustomerImage(lParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultCustomerImage);
            //--009
            //JsonResultlist = JsonResultlist.Substring(0, 500);
            //009--

            var strJson = Core.Services.RestPublisher.Serialize(lParamlist);
            var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
            string sizeKB = size.ToString() + " KB";
            Core.Manager.LogFacade.InsertLog("InsertCustomerImage", lEmployeeID, JsonResultlist, lParamlist.FirstOrDefault().BranchID, lParamlist.FirstOrDefault().deviceID, "", sizeKB);

            return resultCustomerImage;
        }
        //003

        //--004
        public Core.Model.mdlResultList UpdateDeliveryOrder(List<Core.Model.mdlDeliveryOrderParam> lDOParamlist)  //005
        {
            //string serlJson = JsonConvert.SerializeObject(lDOParamlist);
            string lEmployeeID = lDOParamlist.FirstOrDefault().EmployeeID;



            var resultlist = DeliveryOrderFacade.UpdateDeliveryOrder(lDOParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultlist);
            //--009
            //JsonResultlist = JsonResultlist.Substring(0, 500);
            //009--

            var strJson = Core.Services.RestPublisher.Serialize(lDOParamlist);
            var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
            string sizeKB = size.ToString() + " KB";
            Core.Manager.LogFacade.InsertLog("UpdateDeliveryOrder", lEmployeeID, JsonResultlist, lDOParamlist.FirstOrDefault().BranchID, lDOParamlist.FirstOrDefault().deviceID, "", sizeKB);

            return resultlist;
        }

        public Core.Model.mdlResultList UpdateDeliveryOrderDetail(List<Core.Model.mdlDeliveryOrderDetailParam> lDODetailParamlist) //005
        {
            string serlJson = JsonConvert.SerializeObject(lDODetailParamlist);
            string lEmployeeID = lDODetailParamlist.FirstOrDefault().EmployeeID;



            var resultDO = DeliveryOrderFacade.UpdateDeliveryOrderDetail(lDODetailParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultDO);
            //--009
            //JsonResultlist = JsonResultlist.Substring(0, 500);
            //009--

            var strJson = Core.Services.RestPublisher.Serialize(lDODetailParamlist);
            var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
            string sizeKB = size.ToString() + " KB";
            Core.Manager.LogFacade.InsertLog("UpdateDeliveryOrderDetail", lEmployeeID, JsonResultlist, lDODetailParamlist.FirstOrDefault().BranchID, lDODetailParamlist.FirstOrDefault().deviceID, "", sizeKB);

            return resultDO;
        }
        //004--

        public Core.Model.mdlResultList InsertVisit(List<Core.Model.mdlVisitParam> lVisitParamlist)
        {
            //string serlJson = JsonConvert.SerializeObject(lVisitParamlist);
            string lEmployeeID = lVisitParamlist.FirstOrDefault().EmployeeID;



            var resultVisit = VisitFacade.InsertVisit(lVisitParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultVisit);
            //--009
            //JsonResultlist = JsonResultlist.Substring(0, 500);
            //009--

            var strJson = Core.Services.RestPublisher.Serialize(lVisitParamlist);
            var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
            string sizeKB = size.ToString() + " KB";
            Core.Manager.LogFacade.InsertLog("InsertVisit", lEmployeeID, JsonResultlist, lVisitParamlist.FirstOrDefault().BranchID, lVisitParamlist.FirstOrDefault().deviceID, "", sizeKB);

            return resultVisit;
        }

        public Core.Model.mdlResultList InsertLogVisit(List<Core.Model.mdlLogVisitParam> lVisitParamlist)
        {
            //string serlJson = JsonConvert.SerializeObject(lVisitParamlist);
            string lEmployeeID = lVisitParamlist.FirstOrDefault().EmployeeID;



            var resultVisit = LogVisitFacade.InsertLogVisit(lVisitParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultVisit);
            //--009
            //JsonResultlist = JsonResultlist.Substring(0, 500);
            //009--

            var strJson = Core.Services.RestPublisher.Serialize(lVisitParamlist);
            var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
            string sizeKB = size.ToString() + " KB";
            Core.Manager.LogFacade.InsertLog("InsertLogVisit", lEmployeeID, JsonResultlist, lVisitParamlist.FirstOrDefault().BranchID, lVisitParamlist.FirstOrDefault().deviceID, "", sizeKB);

            return resultVisit;
        }

        public Core.Model.mdlResultList InsertVisitDetail(List<Core.Model.mdlVisitDetailParamNew> lVisitDetailParamlist)
        {
            //string serlJson = JsonConvert.SerializeObject(lVisitDetailParamlist);
            string lEmployeeID = lVisitDetailParamlist.FirstOrDefault().EmployeeID;



            var resultVisitDetail = VisitFacade.InsertVisitDetail(lVisitDetailParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(resultVisitDetail);
            //--009
            //JsonResultlist = JsonResultlist.Substring(0, 500);
            //009--

            var strJson = Core.Services.RestPublisher.Serialize(lVisitDetailParamlist);
            var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
            string sizeKB = size.ToString() + " KB";
            Core.Manager.LogFacade.InsertLog("InsertVisitDetail", lEmployeeID, JsonResultlist, lVisitDetailParamlist.FirstOrDefault().BranchID, lVisitDetailParamlist.FirstOrDefault().deviceID, "", sizeKB);

            return resultVisitDetail;
        }
        //006--

        public Core.Model.mdlResultList InsertDailyCost(List<Core.Model.mdlDailyCostParam> lParamlist)
        {
            string serlJson = JsonConvert.SerializeObject(lParamlist);
            string lEmployeeID = "";
            var resultDailyCost = new Core.Model.mdlResultList();

            if (lParamlist.Count > 0)
            {
                lEmployeeID = lParamlist.FirstOrDefault().EmployeeID;



                resultDailyCost = CostFacade.InsertDailyCost(lParamlist);

                string JsonResultlist = JsonConvert.SerializeObject(resultDailyCost);
                //--009
                //JsonResultlist = JsonResultlist.Substring(0, 500);
                //009--

                var strJson = Core.Services.RestPublisher.Serialize(lParamlist);
                var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
                string sizeKB = size.ToString() + " KB";
                Core.Manager.LogFacade.InsertLog("InsertDailyCost", lEmployeeID, JsonResultlist, lParamlist.FirstOrDefault().BranchID, lParamlist.FirstOrDefault().deviceID, "", sizeKB);
            }
            else
            {
                var mdlResultList = new List<Core.Model.mdlResult>();

                var mdlResult = new Core.Model.mdlResult();

                mdlResult.Result = "0";
                mdlResultList.Add(mdlResult);
                resultDailyCost.ResultList = mdlResultList;
            }
            return resultDailyCost;
        }

        public Core.Model.mdlResultList InsertTracking(Core.Model.mdlTrackingParam lParam)
        {
            //string serlJson = JsonConvert.SerializeObject(lParam);
            //string lEmployeeID = lParamlist.FirstOrDefault().EmployeeID;

            //string result = Core.Manager.LogFacade.InsertLog("InsertTracking", serlJson, lParam.EmployeeID, "");



            var resultInsertTracking = TrackingFacade.InsertTracking(lParam);

            //update christopher
            var mdlIdleCounter = IdleCounterFacade.CheckIfIdleCounterExist(lParam.EmployeeID, lParam.BranchID);
            if (mdlIdleCounter.EmployeeID == "" || mdlIdleCounter.EmployeeID == null)
            {
                string resultInsertIdleCounter = IdleCounterFacade.InsertIdleCounter(lParam.EmployeeID, lParam.BranchID, lParam.Latitude, lParam.Longitude, Convert.ToDateTime(lParam.TrackingDate));
            }
            else
            {

                double idleRadius = 0;
                double idleTime = 0;

                List<Core.Model.mdlSettings> listSettings = GeneralSettingsFacade.GetCurrentSettings(lParam.BranchID);
                if (listSettings.Count > 0)
                {
                    foreach (var setting in listSettings)
                    {
                        if (setting.name == "IDLERADIUS")
                        {
                            idleRadius = Convert.ToDouble(setting.value);
                        }
                        else if (setting.name == "IDLETIME")
                        {
                            idleTime = Convert.ToDouble(setting.value);
                        }

                    }
                }

                double baseLatitude = double.Parse(mdlIdleCounter.Latitude, CultureInfo.InvariantCulture);
                double baseLongitude = double.Parse(mdlIdleCounter.Longitude, CultureInfo.InvariantCulture);

                double newLatitude = double.Parse(lParam.Latitude, CultureInfo.InvariantCulture);
                double newLongitude = double.Parse(lParam.Longitude, CultureInfo.InvariantCulture);
                var baseCoor = new GeoCoordinate(baseLatitude, baseLongitude);
                var newCoor = new GeoCoordinate(newLatitude, newLongitude);

                double distance = RadiusFacade.getDistance(baseCoor, newCoor);

                Core.Model.mdlLogIdleParam mdlLogIdle = new Core.Model.mdlLogIdleParam();
                mdlLogIdle.EmployeeID = lParam.EmployeeID;
                mdlLogIdle.BranchID = lParam.BranchID;
                mdlLogIdle.Longitude = lParam.Longitude;
                mdlLogIdle.Latitude = lParam.Latitude;
                mdlLogIdle.StartIdle = mdlIdleCounter.StartDate;
                
                mdlLogIdle.Now = lParam.TrackingDate; // fernandes

                TimeSpan duration = Convert.ToDateTime(mdlLogIdle.Now) - Convert.ToDateTime(mdlLogIdle.StartIdle); // fernandes
                //TimeSpan duration = DateTime.Now.Subtract(Convert.ToDateTime(mdlIdleCounter.StartDate));
                //mdlLogIdle.Duration = duration.ToString(@"hh\:mm\:ss");
                mdlLogIdle.Duration = duration.ToString();
                
                mdlLogIdle.Location = "";
                //mdlLogIdle.Location = ReverseGeocodingFacade.GetStreetName(lParam.Latitude, lParam.Longitude);

                string resultLogIdle = "";

                if (lParam.FlagCheckIn == "True")
                {
                    string resultUpdateBaseCounter = IdleCounterFacade.UpdateBaseIdleCounter(lParam.EmployeeID, lParam.BranchID, lParam.Latitude, lParam.Longitude, Convert.ToDateTime(lParam.TrackingDate));

                    mdlLogIdle.Status = "True";
                    resultLogIdle = LogIdleFacade.UpdateLogIdleClose(mdlLogIdle);
                }
                else
                {
                    if (distance > idleRadius)
                    {
                        //tidak idle

                        mdlLogIdle.Status = "True";
                        resultLogIdle = LogIdleFacade.UpdateLogIdleClose(mdlLogIdle);
                        string resultUpdateBaseCounter = IdleCounterFacade.UpdateBaseIdleCounter(lParam.EmployeeID, lParam.BranchID, lParam.Latitude, lParam.Longitude, Convert.ToDateTime(lParam.TrackingDate));
                    }
                    else
                    {
                        // idle
                        int newCounter = mdlIdleCounter.Counter + 1;
                        string resultUpdateCounter = IdleCounterFacade.UpdateIdleCounter(lParam.EmployeeID, lParam.BranchID, lParam.Latitude, lParam.Longitude, newCounter);

                        var mdlMobileConfig = IdleCounterFacade.GetMobileConfigIdleCounter(lParam.BranchID);
                        if (mdlMobileConfig.ID != null || mdlMobileConfig.ID != "")
                        {
                            double roundMaxCounter = Math.Ceiling(idleTime / Convert.ToDouble(mdlMobileConfig.Value));
                            if (newCounter >= Convert.ToInt32(roundMaxCounter))
                            {
                                string resultInsertIdleLog = IdleCounterFacade.InsertIdleLog(lParam.EmployeeID, lParam.BranchID, "", Convert.ToDateTime(lParam.TrackingDate));

                                mdlLogIdle.Status = "False";
                                var logIdle = LogIdleFacade.GetLogIdle(mdlLogIdle);
                                if (logIdle.BranchID != null)
                                {
                                    resultLogIdle = LogIdleFacade.UpdateLogIdle(mdlLogIdle);
                                }
                                else
                                {

                                    resultLogIdle = LogIdleFacade.InsertLogIdle(mdlLogIdle);
                                }
                            }
                        }

                    }
                }




            }





            //string JsonResultlist = JsonConvert.SerializeObject(resultInsertTracking);
            //--009
            //JsonResultlist = JsonResultlist.Substring(0, 500);
            //009--
            //Core.Manager.LogFacade.InsertLog("InsertTracking", serlJson, lParam.EmployeeID, JsonResultlist);

            return resultInsertTracking;
        }


        //--011
        //public Core.Model.mdlProductList GetProduct()
        //{
        //    var lmdlProduct = new Core.Model.mdlProductList();
        //    lmdlProduct = ProductFacade.LoadProduct();
        //    return lmdlProduct;
        //}
        //011--

        //christopher
        public Core.Model.mdlResult InsertAndroidKey(Core.Model.mdlSaveAndroidKeyParam param)
        {
            return JsonFacade.InsertAndroidKey(param);
        }

        public Core.Model.mdlSetDeviceID SetUserConfig(Core.Model.mdlSetDeviceIDParam param)
        {
            return JsonFacade.SetUserConfig(param);
        }

        public Core.Model.mdlDailyMsgList GetDailyMessage(Core.Model.mdlParam param)
        {
            return JsonFacade.GetDailyMessage(param);
        }

        //public Core.Model.mdlDailyMsgList GetCallPlan(Core.Model.mdlParam param)
        //{
        //    return JsonFacade.GetDailyMessage(param);
        //}



        public List<Core.Model.mdlMobileConfig> GetMobileConfig(Core.Model.mdlParam param)
        {
            return MobileConfigFacade.LoadMobileConfig(param);
        }

        public Core.Model.mdlDailyMsgList GetUserConfig(Core.Model.mdlParam param)
        {
            return JsonFacade.GetDailyMessage(param);
        }



        public Core.Model.mdlCheckinCourierRadius CheckinCourierRadius(Core.Model.mdlCheckinCourierRadiusParam param)
        {
            return JsonFacade.CheckinCourierRadius(param);
        }

        public Core.Model.mdlResult PushNotificationConfirmation(Core.Model.mdlPushNotificationConfirmationParam param)
        {
            return JsonFacade.PushNotificationConfirmation(param);
        }

        public Core.Model.mdlResultList UploadJson(Core.Model.mdlUploadJsonParam lParamlist)
        {
            return Core.Manager.JsonFacade.UploadJson(lParamlist);
        }

        public Core.Model.mdlResult CheckVersion(Core.Model.mdlParam param)
        {
            return Core.Manager.MobileConfigFacade.CheckVersion(param);
        }

        public Core.Model.mdlVehicleList LoadVehicleByBranch(Core.Model.mdlVehicleBranchParam param)
        {
            return Core.Manager.VehicleFacade.LoadVehicleByBranch(param);
        }

        public Core.Model.mdlResultList InsertCostVisit(List<Core.Model.mdlCostVisit> lParamlist)
        {
            string serlJson = JsonConvert.SerializeObject(lParamlist);
            string lEmployeeID = "";
            var resultDailyCost = new Core.Model.mdlResultList();

            if (lParamlist.Count > 0)
            {
                lEmployeeID = lParamlist.FirstOrDefault().EmployeeID;



                resultDailyCost = CostFacade.InsertCostVisit(lParamlist);

                string JsonResultlist = JsonConvert.SerializeObject(resultDailyCost);
                //--009
                //JsonResultlist = JsonResultlist.Substring(0, 500);
                //009--
                var strJson = Core.Services.RestPublisher.Serialize(lParamlist);
                var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
                string sizeKB = size.ToString() + " KB";
                Core.Manager.LogFacade.InsertLog("InsertCostVisit", lEmployeeID, JsonResultlist, lParamlist.FirstOrDefault().BranchID, lParamlist.FirstOrDefault().deviceID, "", sizeKB);
            }
            else
            {
                var mdlResultList = new List<Core.Model.mdlResult>();

                var mdlResult = new Core.Model.mdlResult();

                mdlResult.Result = "0";
                mdlResultList.Add(mdlResult);
                resultDailyCost.ResultList = mdlResultList;
            }
            return resultDailyCost;
        }

        //FERNANDES-RatioBBM Service (12 April 2017)
        public Core.Model.mdlResultList InsertBBMRatio(List<Core.Model.mdlBBMRatioParam> lBBMRatioParamlist)
        {
            string lEmployeeID = lBBMRatioParamlist.FirstOrDefault().EmployeeID;

            var result = BBMFacade.InsertBBMRatio(lBBMRatioParamlist);

            string JsonResultlist = JsonConvert.SerializeObject(result);

            var strJson = Core.Services.RestPublisher.Serialize(lBBMRatioParamlist);
            var size = System.Text.ASCIIEncoding.Unicode.GetByteCount(strJson) / 1024;
            string sizeKB = size.ToString() + " KB";
            Core.Manager.LogFacade.InsertLog("InsertRatioBBM", lEmployeeID, JsonResultlist, lBBMRatioParamlist.FirstOrDefault().BranchID, lBBMRatioParamlist.FirstOrDefault().DeviceID, "", sizeKB);

            return result;
        }

        public Core.Model.mdlKnox LoadKnoxKey(Core.Model.mdlKnoxParam param)
        {
            return KnoxFacade.LoadKnoxKey(param);
        }

        //--------------------------------------------- Close Code -----------------------------------------------------------//

        // public Core.Model.mdlResult InsertCustomer(Core.Model.mdlCustomerParam lParam)
        // {
        //     return CustomerFacade.InsertCustomer(lParam);
        // }

        // public Core.Model.mdlResult UpdateCustomer(Core.Model.mdlCustomerParam lParam)
        // {
        //     return CustomerFacade.UpdateCustomer(lParam);
        // }

        // public Core.Model.mdlResult DeleteCustomer(Core.Model.mdlCustomerParam lParam)
        // {
        //     return CustomerFacade.DeleteCustomer(lParam);
        // }

        // //CustomerType
        // public List<Core.Model.mdlCustomerType> LoadCustomerType()
        // {
        //     return CustomerTypeFacade.LoadCustomerType();
        // }

        // public Core.Model.mdlResult InsertCustomerType(Core.Model.mdlCustomerTypeParam lParam)
        // {
        //     return CustomerTypeFacade.InsertCustomerType(lParam);
        // }

        // public Core.Model.mdlResult UpdateCustomerType(Core.Model.mdlCustomerTypeParam lParam)
        // {
        //     return CustomerTypeFacade.UpdateCustomerType(lParam);
        // }

        // public Core.Model.mdlResult DeleteCustomerType(Core.Model.mdlCustomerTypeParam lParam)
        // {
        //     return CustomerTypeFacade.DeleteCustomerType(lParam);
        // }

        // //Branch
        // public Core.Model.mdlBranchList LoadBranch()
        // {
        //     return BranchFacade.LoadBranch();
        // }

        // public Core.Model.mdlResult InsertBranch(Core.Model.mdlBranchParam lParam)
        // {
        //     return BranchFacade.InsertBranch(lParam);
        // }

        // public Core.Model.mdlResult UpdateBranch(Core.Model.mdlBranchParam lParam)
        // {
        //     return BranchFacade.UpdateBranch(lParam);
        // }

        // public Core.Model.mdlResult DeleteBranch(Core.Model.mdlBranchParam lParam)
        // {
        //     return BranchFacade.DeleteBranch(lParam);
        // }

        // //Company
        // public Core.Model.mdlCompanyList LoadCompany()
        // {
        //     return CompanyFacade.LoadCompany();
        // }

        // public Core.Model.mdlResult InsertCompany(Core.Model.mdlCompanyParam lParam)
        // {
        //     return CompanyFacade.InsertCompany(lParam);
        // }

        // public Core.Model.mdlResult UpdateCompany(Core.Model.mdlCompanyParam lParam)
        // {
        //     return CompanyFacade.UpdateCompany(lParam);
        // }

        // public Core.Model.mdlResult DeleteCompany(Core.Model.mdlCompanyParam lParam)
        // {
        //     return CompanyFacade.DeleteCompany(lParam);
        // }

        // //Product
        // //public List<Core.Model.mdlProduct> LoadProduct()
        // //{
        // //    return ProductFacade.LoadProduct();
        // //}

        // public Core.Model.mdlResult InsertProduct(Core.Model.mdlProductParam lParam)
        // {
        //     return ProductFacade.InsertProduct(lParam);
        // }

        // public Core.Model.mdlResult UpdateProduct(Core.Model.mdlProductParam lParam)
        // {
        //     return ProductFacade.UpdateProduct(lParam);
        // }

        // public Core.Model.mdlResult DeleteProduct(Core.Model.mdlProductParam lParam)
        // {
        //     return ProductFacade.DeleteProduct(lParam);
        // }

        // //public List<Core.Model.PurchaseOrder> ListPO()
        // //{
        // //    return PurchaseOrderFacade.LoadPurchaseOrder();
        // //}

        // //public Core.Model.mdlVisitList GetVisit(Core.Model.mdlParam json)
        // //{
        // //    return VisitFacade.LoadVisit(json);
        // //}

        // //public Core.Model.mdlVisitList LoadVisit()
        // //{
        // //    return VisitFacade.LoadVisit1();
        // //}

        // //public Core.Model.mdlCustomerList GetCustomer(Core.Model.mdlParam json)
        // //{
        // //    return CustomerFacade.LoadCustomer(json);
        // //}

        ////public Core.Model.mdlDe_cons_content_staList GetConNote(Core.Model.mdlParam json)
        ////{
        ////    return Core.Manager.JsonFacade.LoadConNote(json);
        ////}

        ////public Core.Model.mdlResultList InsertConNote(List<Core.Model.mdlDrsMobile> drs)
        ////{
        ////    return De_cons_content_staFacade.InsertDrsMobile(drs);
        ////}

        ////public Core.Model.mdlDe_mdeList GetConnote(Core.Model.mdlDrsMobile json)
        ////{
        ////    var mdlDrsM = new Core.Model.DrsMobile();

        ////    int checkLength = json.Track_no.Length;
        ////    if (checkLength == 12)
        ////    {

        ////        mdlDrsM.track_no = json.Track_no;
        ////        mdlDrsM.drs_id = json.Drs_id;
        ////        mdlDrsM.seq_no = 0;
        ////        mdlDrsM.courier_code = json.Courier_code;
        ////        mdlDrsM.sta_code = json.Sta_code;

        ////    }
        ////    else if (checkLength == 13)
        ////    {
        ////        string connoteID = json.Track_no;
        ////        string splitConnoteId = connoteID.Substring(0, 12);
        ////        int splitSeqId = Convert.ToInt16(connoteID.Substring(12, 1));

        ////        mdlDrsM.track_no = splitConnoteId;
        ////        mdlDrsM.drs_id = json.Drs_id;
        ////        mdlDrsM.seq_no = splitSeqId;
        ////        mdlDrsM.courier_code = json.Courier_code;
        ////        mdlDrsM.sta_code = json.Sta_code;
        ////    }


        ////    Core.Manager.DrsMobileFacade.InsertConnote(mdlDrsM);
        ////    return Core.Manager.JsonFacade.LoadConnote(mdlDrsM);



        ////}

        //public Core.Model.mdlResult InsertEmployee(Core.Model.mdlEmployeeParam lParam)
        //{
        //    return EmployeeFacade.InsertEmployee(lParam);
        //}

        //public Core.Model.mdlResult UpdateEmployee(Core.Model.mdlEmployeeParam lParam)
        //{
        //    return EmployeeFacade.UpdateEmployee(lParam);
        //}

        //public Core.Model.mdlResult DeleteEmployee(Core.Model.mdlEmployeeParam lParam)
        //{
        //    return EmployeeFacade.DeleteEmployee(lParam);
        //}


        //public Core.Model.mdlResult InsertEmployeeType(Core.Model.mdlEmployeeTypeParam lParam)
        //{
        //    return EmployeeTypeFacade.InsertEmployeeType(lParam);
        //}

        //public Core.Model.mdlResult UpdateEmployeeType(Core.Model.mdlEmployeeTypeParam lParam)
        //{
        //    return EmployeeTypeFacade.UpdateEmployeeType(lParam);
        //}

        //public Core.Model.mdlResult DeleteEmployeeType(Core.Model.mdlEmployeeTypeParam lParam)
        //{
        //    return EmployeeTypeFacade.DeleteEmployeeType(lParam);
        //}

        // //--FERNANDES
        //public Core.Model.mdlVehicleList LoadVehicle()
        //{
        //    return VehicleFacade.LoadVehicle();
        //}//FERNANDES--

        //public Core.Model.mdlResult InsertVehicle(Core.Model.mdlVehicleParam lParam)
        //{
        //    return VehicleFacade.InsertVehicle(lParam);
        //}

        //public Core.Model.mdlResult UpdateVehicle(Core.Model.mdlVehicleParam lParam)
        //{
        //    return VehicleFacade.UpdateVehicle(lParam);
        //}

        //public Core.Model.mdlResult DeleteVehicle(Core.Model.mdlVehicleParam lParam)
        //{
        //    return VehicleFacade.DeleteVehicle(lParam);
        //}


        ////--FERNANDES
        //public Core.Model.mdlVehicleTypeList LoadVehicleType()
        //{
        //    return VehicleTypeFacade.LoadVehicleType();
        //}//FERNANDES--

        //public Core.Model.mdlResult InsertVehicleType(Core.Model.mdlVehicleTypeParam lParam)
        //{
        //    return VehicleTypeFacade.InsertVehicleType(lParam);
        //}

        //public Core.Model.mdlResult UpdateVehicleType(Core.Model.mdlVehicleTypeParam lParam)
        //{
        //    return VehicleTypeFacade.UpdateVehicleType(lParam);
        //}

        //public Core.Model.mdlResult DeleteVehicleType(Core.Model.mdlVehicleTypeParam lParam)
        //{
        //    return VehicleTypeFacade.DeleteVehicleType(lParam);
        //}

        ////nanda
        //public Core.Model.mdlEmployeeList LoadEmployee()
        //{
        //    return EmployeeFacade.LoadEmployee();
        //}

        //public Core.Model.mdlEmployeeTypeList LoadEmployeeType()
        //{
        //    return EmployeeTypeFacade.LoadEmployeeType();
        //}
        ////nanda
    }
}
