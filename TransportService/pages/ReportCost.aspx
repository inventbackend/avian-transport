﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="ReportCost.aspx.cs" Inherits="TransportService.pages.ReportCost" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Report Cost</h3>
        </div>
        <div class="title_right">
            <%--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>--%>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
           <%-- <div class="x_title">
                <h2>
                    Form Basic Elements <small>different form elements</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>--%>
            <div class="x_content">
                <br />
                <div class="form-group">
                    <fieldset>
                        <asp:ScriptManager ID="ScriptManager2" runat="server">
                        </asp:ScriptManager>
                        <p>
                            <asp:Label ID="lblBranchD" runat="server" Text="Branch ID" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                            <asp:Label ID="Label2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                            <%--<asp:TextBox ID="txtBranchID" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12" placeholder="Branch ID"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                ControlToValidate="txtBranchID" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <ajaxToolkit:AutoCompleteExtender ID="txtBranchID_AutoCompleteExtender" runat="server"
                                ServiceMethod="SearchBranchID" MinimumPrefixLength="2" EnableCaching="false"
                                DelimiterCharacters="" Enabled="True" TargetControlID="txtBranchID">
                            </ajaxToolkit:AutoCompleteExtender>--%>
                            <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Label ID="lblEmployeeID" runat="server" Text="Employee ID" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                            <asp:Label ID="Label4" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                            <asp:TextBox ID="txtEmployeeID" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Search Employee"></asp:TextBox>
                            <%--001 nanda--%>
                            <%--<ajaxToolkit:AutoCompleteExtender ID="txtEmployeeID_AutoCompleteExtender" runat="server"
                                                    ServiceMethod="SearchEmployeeID" 
                                                    MinimumPrefixLength="2" 
                                                    EnableCaching="false" 
                                                    DelimiterCharacters="" 
                                                    Enabled="True" 
                                                    TargetControlID="txtEmployeeID"> 
                                                    </ajaxToolkit:AutoCompleteExtender>--%>
                            <asp:Button ID="btnShowEmployee" runat="server" CssClass="btn btn-success" Text="Show Employee"
                                OnClick="btnShowEmployee_Click" style="margin-left:10px"/>
                            <asp:Label ID="lblValblEmployee" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                            <%-- 002 fernandes --%>
                            <asp:Panel ID="Panel1" CssClass="form-control" Height="200px" Width="400px" runat="server"
                                ScrollBars="Vertical" style="margin-left:10px">
                                <asp:CheckBoxList ID="blEmployee" runat="server">
                                </asp:CheckBoxList>
                            </asp:Panel>
                            <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success" Text="SELECT ALL"
                                OnClick="btnSelectAll_Click" style="margin-left:10px"/>
                            <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success" Text="UNSELECT ALL"
                                OnClick="btnUnSelectAll_Click" style="margin-left:10px"/>
                        </p>
                        <%--       
                                                    <asp:Button ID="btnShowBranch" runat="server" Text="Show Branch" Style="margin-left: 0px;
                                                        margin-top: 0px" CssClass="myButton" OnClick="btnShowBranch_Click"></asp:Button>
                                                    <asp:TextBox ID="txtSearchBranch" runat="server" Style="margin-left: 0px; margin-top: 0px"
                                                        Width="200px" CssClass="tb5"></asp:TextBox>

                                                    <ajaxToolkit:AutoCompleteExtender ID="txtBranchName_AutoCompleteExtender" runat="server"
                                                    ServiceMethod="SearchBranchName" 
                                                    MinimumPrefixLength="2" 
                                                    EnableCaching="false" 
                                                    DelimiterCharacters="" 
                                                    Enabled="True" 
                                                    TargetControlID="txtSearchBranch"> 
                                                    </ajaxToolkit:AutoCompleteExtender>--%>
                        <%--<asp:Button ID="btnSearch" runat="server" Text="Search Branch" Style="margin-left: 0px;
                                                        margin-top: 0px"  CssClass="myButton"></asp:Button>
                                                    <asp:Button ID="btnCancelBranch" runat="server" Text="Cancel" Style="margin-left: 0px;
                                                        margin-top: 0px" CssClass="myButton" ></asp:Button>--%>
                        <%-- <asp:Panel ID="PanelBranch" runat="server" Width="100%" Height="80px" ScrollBars="Vertical"
                                                    CssClass="datagrid">
                                                    <table style="width: 100%;">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    Branch ID
                                                                </th>
                                                                <th>
                                                                    Branch Name
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <asp:Repeater ID="RptBranchlist" runat="server" OnItemCommand="rptBranch_ItemCommand">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:LinkButton ID="lnkBranchId" runat="server" CommandName="Link" Text='<%# Eval("BranchID") %>'></asp:LinkButton>
                                                                        </td>
                                                                        <td>
                                                                            <%# Eval("BranchName")%>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tbody>
                                                    </table>
                                                </asp:Panel>--%>
                        <p>
                            <asp:Label ID="lbl2" runat="server" Text="Date" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                            <asp:Label ID="Label3" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                            <asp:TextBox ID="txtdate1" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="StartDate"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="- Start Date Required -"
                                ControlToValidate="txtdate1" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <script type="text/javascript">
                                var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtdate1.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                            </script>
                            <%--<asp:Label ID="Label1" runat="server" Text="To" Width="50px" Style="text-align: center"></asp:Label>--%>
                            <asp:TextBox ID="txtdate2" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="End Date" style="margin-left:10px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="- End Date Required -"
                                ControlToValidate="txtdate2" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                SetFocusOnError="True" PlaceHolder="EndDate"></asp:RequiredFieldValidator>
                            <script type="text/javascript">
                                var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtdate2.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                            </script>
                        </p>
                        <br />
                        <asp:Button ID="btnShow" runat="server" Text="Show Report PDF" class="btn btn-sm btn-embossed btn-primary"
                            Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                            <asp:Button ID="btnShowExcel" runat="server" Text="Show Report Excel" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShowExcel_Click" ValidationGroup="valShowReport" />
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <%-----------------------------------------------------------// Start Cut Form //--------------------------------------------------------------------------------------%>
        <div class="col-md-12 col-xs-12">
        <asp:Panel ID="PanelReportCost" runat="server" Height="500px" Width="100%" CssClass="datagrid">
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                WaitMessageFont-Size="14pt">
                <LocalReport ReportPath="Report\ReportCost.rdlc">
                </LocalReport>
            </rsweb:ReportViewer>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
