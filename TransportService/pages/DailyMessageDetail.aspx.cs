﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class DailyMessageDetail : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        private void ClearContent()
        {
            txtMsgID.Text = string.Empty;
            //txtBranch.Text = string.Empty;
            //txtSearchBranch.Text = string.Empty;
            lblValbl.Visible = false;

            var lEmployeebllist = EmployeeFacade.LoadEmployeelistReport(ddlSearchBranchID.SelectedValue);
            blEmployee.DataSource = lEmployeebllist;
            blEmployee.DataBind();
            
            PanelBranch.Visible = false;

            btnCancel.Visible = false;
            btnInsert.Visible = false;
            
            PanelFormDailyMsg.Visible = false;
        }

        public string lParam { get; set; }

        private void BindData(string keyword, string keyword2, string branchid)
        {
            var listDailyMsgRole = new List<Core.Model.mdlDailyMsgRole>();

            if (getBranch() == "")
            {
                listDailyMsgRole = Core.Manager.DailyMessageFacade.GetData(keyword, keyword2, branchid);
            }
            else
            {
                listDailyMsgRole = Core.Manager.DailyMessageFacade.GetDataBySomeBranch(keyword, keyword2, branchid);
            }

            PagedDataSource pgitems = new PagedDataSource();
            //DataView dv = new DataView(dt);
            pgitems.DataSource = listDailyMsgRole;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;
            //lblPage.Text = pgitems.PageCount.ToString();
            if (pgitems.PageCount > 1)
            {
                lnkNext.Visible = true;
                lnkPrevious.Visible = true;
                rptPaging.Visible = true;

                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }

            RptDailyMsglist.DataSource = pgitems;
            RptDailyMsglist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string id = Request.QueryString["cid"];
                
                BindData(id,"",getBranch());
                ClearContent();
            }
        }

        protected void rptDailyMsg_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                Control control,control2,control3;
                control = e.Item.FindControl("MsgID");
                control2 = e.Item.FindControl("EmployeeID");
                control3 = e.Item.FindControl("BranchID");

                if (control != null && control2 != null && control3 != null)
                {
                    string id,id2,id3;
                    id = ((Label)control).Text;
                    id2 = ((Label)control2).Text;
                    id3 = ((Label)control3).Text;

                    DailyMessageFacade.DeleteMessageDetail(id,id2,id3);

                    string status = Request.QueryString["cid"];

                    BindData(id, txtSearch.Text,getBranch());
                }
            }
        }

        //001
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string id = Request.QueryString["cid"];

            if (getBranch() == "")
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
                //txtBranch.Text = string.Empty;
                //txtBranch.ReadOnly = false;
                //btnShowBranch.Enabled = true;
                //btnSearch.Enabled = true;
                //txtSearchBranch.ReadOnly = false;
                //btnCancelBranch.Enabled = true;
            }
            else
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
                //txtBranch.Text = getBranch();
                //txtBranch.ReadOnly = true;
                //btnShowBranch.Enabled = false;
                //btnSearch.Enabled = false;
                //txtSearchBranch.ReadOnly = true;
                //btnCancelBranch.Enabled = false;
            }

            txtMsgID.ReadOnly = true;
            txtMsgID.Text = id;
            //txtBranch.Text = string.Empty;
            //txtSearchBranch.Text = string.Empty;
            lblValbl.Visible = false;
            blEmployee.Visible = false;

            //var lEmployeebllist = EmployeeFacade.LoadEmployeelistReport(txtBranch.Text);
            //blEmployee.DataSource = lEmployeebllist;
            //blEmployee.DataBind();

            btnCancel.Visible = true;
            btnInsert.Visible = true;
            
            PanelFormDailyMsg.Visible = true;
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string id = Request.QueryString["cid"];
            
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindData(id, txtSearch.Text,getBranch());
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            string id = Request.QueryString["cid"];

            PageNumber = PageNumber - 1;
            BindData(id, txtSearch.Text,getBranch());
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            string id = Request.QueryString["cid"];

            PageNumber = PageNumber + 1;
            BindData(id, txtSearch.Text,getBranch());
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            lParam = "search";
            PageNumber = 0;
            string id = Request.QueryString["cid"];
            BindData(id, txtSearch.Text, getBranch());
        }

        //private void BindDataBranch()
        //{
        //    RptBranchlist.DataSource = BranchFacade.GetSearch(txtSearchBranch.Text , "");
        //    RptBranchlist.DataBind();
        //}

        //protected void rptBranch_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    if (e.CommandName == "Link")
        //    {
        //        Control control;
        //        control = e.Item.FindControl("lnkBranchId");
        //        if (control != null)
        //        {
        //            string id;
        //            id = ((LinkButton)control).Text;
        //            txtBranch.Text = id;
        //            PanelBranch.Visible = false;
        //        }
        //    }
        //}

        //protected void btnShowBranch_Click(object sender, EventArgs e)
        //{
        //    BindDataBranch();
        //    PanelBranch.Visible = true;
        //    txtSearchBranch.Text = string.Empty;
        //    btnCancelBranch.Visible = true;
        //}

        protected void btnShowEmployee_Click(object sender, EventArgs e)
        {
            blEmployee.Visible = true;

            var lEmployeebllist = EmployeeFacade.LoadEmployeelistReport(ddlSearchBranchID.SelectedValue);
            blEmployee.DataSource = lEmployeebllist;
            blEmployee.DataTextField = "EmployeeName";
            blEmployee.DataValueField = "EmployeeID";
            blEmployee.DataBind();
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = true;
            }
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = false;
            }
        }

        //protected void btnSearch_Click(object sender, EventArgs e)
        //{
        //    BindDataBranch();
        //    PanelBranch.Visible = true;
        //    btnCancelBranch.Visible = true;
        //}

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("DailyMessage.aspx");
        }

        //protected void btnCancelBranch_Click(object sender, EventArgs e)
        //{
        //    PanelBranch.Visible = false;
        //    txtBranch.Text = string.Empty;
        //    txtSearchBranch.Text = string.Empty;
        //}

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> blStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in blEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blStrList.Add(item.Value);
                }
            }

            //--Validasi CheckBox List
            if (blStrList.Count == 0)
            {
                lblValbl.Text = "*Required";
                lblValbl.Visible = true;

                return;
            }
            else
            {
                lblValbl.Visible = false;
            }
            //--

            DailyMessageFacade.InsertDailyMessageDetail(txtMsgID.Text, ddlSearchBranchID.SelectedValue, blStrList);
            Response.Redirect("DailyMessageDetail.aspx?cid=" + txtMsgID.Text);

            ClearContent();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        
    }
}