﻿/* documentation
 * 001 20 Okt'16 fernandes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Core.Manager;
using System.IO;
using System.Net;
using System.Text;

namespace TransportService.pages
{
    //--001
    public partial class ReportCost : System.Web.UI.Page
    {
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        private void ClearContent()
        {
            if (getBranch() == "")
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
                //txtBranchID.Text = string.Empty;
                //txtBranchID.ReadOnly = false;
            }
            else
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
                //txtBranchID.Text = getBranch();
                //txtBranchID.ReadOnly = true;
            }

            //txtBranchID.Text = string.Empty;
            PanelReportCost.Visible = false;
            Panel1.Visible = false;
            btnSelectAll.Visible = false;
            btnUnSelectAll.Visible = false;
            txtdate1.Text = DateTime.Now.ToString("MM/dd/yyyy");
            txtdate2.Text = DateTime.Now.ToString("MM/dd/yyyy");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                PanelReportCost.Visible = false;
                ClearContent();
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in blEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }

            //--003
            if (blEmployeeStrList.Count == 0)
            {
                lblValblEmployee.Text = "*Required";
                lblValblEmployee.Visible = true;
                PanelReportCost.Visible = false;

                return;
            }
            else
            {
                lblValblEmployee.Visible = false;
            }
            //003--

            try
            {
                var ds = CostFacade.LoadCostReport(ddlSearchBranchID.SelectedValue, Convert.ToDateTime(txtdate1.Text), Convert.ToDateTime(txtdate2.Text), blEmployeeStrList);
                ReportDataSource rds = new ReportDataSource("dsCost", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }
            catch
            {
                DateTime txtdateStart = DateTime.ParseExact(txtdate1.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime txtdateEnd = DateTime.ParseExact(txtdate2.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                var ds = CostFacade.LoadCostReport(ddlSearchBranchID.SelectedValue, txtdateStart, txtdateEnd, blEmployeeStrList);
                ReportDataSource rds = new ReportDataSource("dsCost", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportCost.rdlc");

            ReportParameter[] parameter = new ReportParameter[2];
            parameter[0] = new ReportParameter("StartDate", txtdate1.Text);
            parameter[1] = new ReportParameter("EndDate", txtdate2.Text);
            ReportViewer1.LocalReport.SetParameters(parameter);

            ReportViewer1.LocalReport.Refresh();

            //PanelReportCost.Visible = true;
            ReportViewer1.Visible = false;

            //preview pdf
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            FileStream fs = new FileStream(Server.MapPath(@"~\report.pdf"), FileMode.Create);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();

            string url = "PDF_Report.aspx";

            StringBuilder sb = new StringBuilder();

            sb.Append("<script type = 'text/javascript'>");

            sb.Append("window.open('");

            sb.Append(url);

            sb.Append("');");

            sb.Append("</script>");

            ClientScript.RegisterStartupScript(this.GetType(),

                    "script", sb.ToString());
        }

        protected void btnShowExcel_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in blEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }

            //--003
            if (blEmployeeStrList.Count == 0)
            {
                lblValblEmployee.Text = "*Required";
                lblValblEmployee.Visible = true;
                PanelReportCost.Visible = false;

                return;
            }
            else
            {
                lblValblEmployee.Visible = false;
            }
            //003--

            try
            {
                var ds = CostFacade.LoadCostReport(ddlSearchBranchID.SelectedValue, Convert.ToDateTime(txtdate1.Text), Convert.ToDateTime(txtdate2.Text), blEmployeeStrList);
                ReportDataSource rds = new ReportDataSource("dsCost", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }
            catch
            {
                DateTime txtdateStart = DateTime.ParseExact(txtdate1.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime txtdateEnd = DateTime.ParseExact(txtdate2.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                var ds = CostFacade.LoadCostReport(ddlSearchBranchID.SelectedValue, txtdateStart, txtdateEnd, blEmployeeStrList);
                ReportDataSource rds = new ReportDataSource("dsCost", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportCost.rdlc");

            ReportParameter[] parameter = new ReportParameter[2];
            parameter[0] = new ReportParameter("StartDate", txtdate1.Text);
            parameter[1] = new ReportParameter("EndDate", txtdate2.Text);
            ReportViewer1.LocalReport.SetParameters(parameter);

            ReportViewer1.LocalReport.Refresh();

            PanelReportCost.Visible = true;

            //download excel file
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportBiayaHarian" + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBranchID(string prefixText, int count)
        {
            List<string> lBranchs = new List<string>();
            lBranchs = BranchFacade.AutoComplBranch(prefixText, count, "BranchID");
            return lBranchs;
        }

        protected void btnShowEmployee_Click(object sender, EventArgs e)
        {
            //--002,004
            if (txtEmployeeID.Text == "")
            {
                var lEmployeelist = EmployeeFacade.LoadEmployeelistReport(ddlSearchBranchID.SelectedValue);
                blEmployee.DataSource = lEmployeelist;
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();
            }
            else
            {
                var lEmployeelist = EmployeeFacade.LoadEmployeelistReport2(txtEmployeeID.Text,ddlSearchBranchID.SelectedValue); //001
                blEmployee.DataSource = lEmployeelist;
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();
            }
            //002,004--


            Panel1.Visible = true;
            btnSelectAll.Visible = true;
            btnUnSelectAll.Visible = true;
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = true;
            }
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = false;
            }
        }

    }
    //001--
}
//------------------------------------------ Close Code ------------------------------------------------------------//
//[System.Web.Script.Services.ScriptMethod()]
//        [System.Web.Services.WebMethod]
//        public static List<string> SearchEmployeeID(string prefixText, int count)
//        {
//            List<string> lEmployees = new List<string>();
//            lEmployees = EmployeeFacade.AutoComplEmployee(prefixText, count, "EmployeeID");
//            return lEmployees;
//        }