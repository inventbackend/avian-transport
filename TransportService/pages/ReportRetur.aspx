﻿<%@ Page Title="Report Retur" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="ReportRetur.aspx.cs" Inherits="TransportService.pages.ReportRetur" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Report Retur</h3>
        </div>
        <div class="title_right">
            <%--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>--%>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <%--<div class="x_title">
                <h2>
                    Form Basic Elements <small>different form elements</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>--%>
            <div class="x_content">
                <br />
                                <div class="form-group">
                                            <asp:ScriptManager ID="ScriptManager2" runat="server">
                                            </asp:ScriptManager>
                                                <p>
                                                    <asp:Label ID="lblBranchD" runat="server" Text="Branch ID" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                                    <asp:Label ID="Label2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                                    <asp:TextBox ID="txtBranchID" runat="server" 
                                                        Width="150px" CssClass="form-control col-md-3 col-xs-12" placeholder="Branch ID"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                            ControlToValidate="txtBranchID" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:AutoCompleteExtender ID="txtBranchID_AutoCompleteExtender" runat="server"
                                                                ServiceMethod="SearchBranchID" 
                                                                MinimumPrefixLength="2" 
                                                                EnableCaching="false" 
                                                                DelimiterCharacters="" 
                                                                Enabled="True" 
                                                                TargetControlID="txtBranchID"> 
                                                                </ajaxToolkit:AutoCompleteExtender>
                                                </p>
                                                <br />
                                                <p>
                                                    <asp:Label ID="lblEmployeeID" runat="server" Text="Employee ID" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                                    <asp:Label ID="Label4" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                                    <asp:TextBox ID="txtEmployeeID" runat="server" 
                                                        Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Search Employee"></asp:TextBox> <%--001 nanda--%>
                                                    <asp:Button ID="btnShowEmployee" Style="margin-left: 9px; margin-top: 0px" runat="server" CssClass="btn btn-success " Text="Show Employee" onclick="btnShowEmployee_Click" />
                                                    <asp:Label ID="lblValblEmployee" runat="server" style="color:Red;" visible="false"></asp:Label> <%-- 002 fernandes --%>
                                                    <asp:Panel ID="Panel1" CssClass="form-control" Height="100px" Width="400px" runat="server" ScrollBars="Vertical">
                                        
                                                    <asp:CheckBoxList ID="blEmployee" runat="server">
                                                    </asp:CheckBoxList>
                                        
                                                    </asp:Panel> 
                                                    <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success " Text="SELECT ALL" onclick="btnSelectAll_Click" />
                                                     <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success " Text="UNSELECT ALL" onclick="btnUnSelectAll_Click" />
                                                </p>
                                                <p>
                                                    <asp:Label ID="lbl2" runat="server" Text="Date" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                                    <asp:Label ID="Label3" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                                    <asp:TextBox ID="txtdate1" runat="server" 
                                                        Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="StartDate"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="- Start Date Required -"
                                                            ControlToValidate="txtdate1" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <script type="text/javascript">
                                                        var picker1 = new Pikaday(
                                                             {
                                                                 field: document.getElementById('<%=txtdate1.ClientID%>'),
                                                                 firstday: 1,
                                                                 minDate: new Date('2000-01-01'),
                                                                 maxDate: new Date('2020-12-31'),
                                                                 yearRange: [2000, 2020]
                                                                 //                                    setDefaultDate: true,
                                                                 //                                    defaultDate : new Date()
                                                             }
                                                                                      );
                                                    </script>
                                                    <%--<asp:Label ID="Label1" runat="server" Text="To" Width="50px" 
                                                        style="text-align: center"></asp:Label>--%>
                                                    <asp:TextBox ID="txtdate2" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="End Date" style="margin-left:10px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="- End Date Required -"
                                                            ControlToValidate="txtdate2" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                            SetFocusOnError="True" PlaceHolder="EndDate"></asp:RequiredFieldValidator>
                                                    <script type="text/javascript">
                                                        var picker1 = new Pikaday(
                                                             {
                                                                 field: document.getElementById('<%=txtdate2.ClientID%>'),
                                                                 firstday: 1,
                                                                 minDate: new Date('2000-01-01'),
                                                                 maxDate: new Date('2020-12-31'),
                                                                 yearRange: [2000, 2020]
                                                                 //                                    setDefaultDate: true,
                                                                 //                                    defaultDate : new Date()
                                                             }
                                                                                      );
                                                    </script>
                                                </p>
                                                <br />
                                                <asp:Button ID="btnShow" runat="server" Text="Show Report" class="btn btn-sm btn-embossed btn-primary"
                                                 Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                            </div>

            <%-----------------------------------------------------------// Start Cut Form //--------------------------------------------------------------------------------------%>

        <asp:Panel ID="PanelReportRetur" runat="server" Height="500px" Width="100%" CssClass="datagrid">          
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                WaitMessageFont-Size="14pt">
                <LocalReport ReportPath="Report\ReportRetur.rdlc">
                </LocalReport>
            </rsweb:ReportViewer>
        </asp:Panel>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
