﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SettingKoordinatWarehousePop.aspx.cs" Inherits="TransportService.pages.SettingKoordinatWarehousePop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="../css/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="../css/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="../css/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="../css/bootstrap3-wysihtml5.min.css" rel="stylesheet"
        type="text/css" />
    <!-- Theme style -->
    <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />

    <link href="../js/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4a8ijWorZUkJKOtzheGKtV1RlyfDYW24&callback=initMap"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>--%>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry,places"></script>--%>
    <!-- Support Jquery -->
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script src="../js/MapDefault.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/GoogleMap.js"></script>
    <script src="../js/mapwithmarker.js" type="text/javascript"></script>
    <style type="text/css">
        .labels
        {
            color: black;
            background-color: #FF8075;
            font-family: Arial;
            font-size: 11px;
            font-weight: bold;
            text-align: center;
            width: 25px;
        }
    </style>

    <script type="text/javascript">

        function loadMap() {

            //Create Map
            objMap = new google.maps.Map(document.getElementById("map"), objGoogleMapOption);

        }

        $(function () {
            loadMap();
        });

        function loadMap2() {

            //Create Map
            objMap = new google.maps.Map(document.getElementById("map2"), objGoogleMapOption);

        }

        $(function () {
            loadMap2();
        });

    </script>

    <script type="text/javascript">
var markers = [
<asp:Repeater ID="rptMarkers" runat="server">
<ItemTemplate>
           {
            "title": '<%# Eval("Date") %>',
            "lat": '<%# Eval("Latitude") %>',
            "lng": '<%# Eval("Longitude") %>',
            "description": '<%# Eval("Date") %>'
        }
</ItemTemplate>
<SeparatorTemplate>
    ,
</SeparatorTemplate>
</asp:Repeater>
];

var visit = [
<asp:Repeater ID="rptVisit" runat="server">
<ItemTemplate>
           
         {
            "title": '<%# Eval("WarehouseName") %>',
            "lat": '<%# Eval("Latitude") %>',
            "lng": '<%# Eval("Longitude") %>',
            "description": '<%# Eval("WarehouseName") %>'
        }
</ItemTemplate>
<SeparatorTemplate>
    ,
</SeparatorTemplate>
</asp:Repeater>
];
    </script>
    
    <script type="text/javascript">
        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };



            var path = new google.maps.MVCArray();
            var service = new google.maps.DirectionsService();
            var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
            var infoWindow = new google.maps.InfoWindow();

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            for (i = 0; i < markers.length; i++) {

                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);

                var dataMaster = visit[0];
                var masterLatlng = new google.maps.LatLng(dataMaster.lat, dataMaster.lng);

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title,
                    icon: "img/shop-red.png"
                });

                var master = new google.maps.Marker({
                    position: masterLatlng,
                    map: map,
                    title: dataMaster.title,
                    icon: "img/shop-green.png"
                });

                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }

            var mapOptions2 = {
                center: new google.maps.LatLng(visit[0].lat, visit[0].lng),
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map2 = new google.maps.Map(document.getElementById("map2"), mapOptions2);

            for (j = 0; j < visit.length; j++) {
                var data2 = visit[j];
                var myLatlng2 = new google.maps.LatLng(data2.lat, data2.lng);
                var marker2 = new google.maps.Marker({
                    draggable: true,
                    position: myLatlng2,
                    map: map2,
                    title: data2.title,
                    icon: "img/shop-green.png"
                });
                google.maps.event.addListener(marker2, 'dragend', function (event) {
                    document.getElementById("latbox").value = this.getPosition().lat();
                    document.getElementById("lngbox").value = this.getPosition().lng();
                });
                (function (marker2, data2) {
                    google.maps.event.addListener(marker2, "click", function (e) {
                        infoWindow.setContent(data2.description);
                        infoWindow.open(map2, marker2);
                    });
                })(marker2, data2);
            }

            //            var lat_lng = new Array();
            //            for (j = 0; j < tracking.length; j++) {
            //                var dataSrc = tracking[j];
            //                var start = new google.maps.LatLng(dataSrc.lat, dataSrc.lng);
            //                // var map = new google.maps.Map(document.getElementById("map"), myOptions);
            //                lat_lng.push(start);
            //                var marker1 = new google.maps.Marker({
            //                    position: start,
            //                    map: map,
            //                    title: dataSrc.title
            //                });
            //                (function (marker1, dataSrc) {
            //                    google.maps.event.addListener(marker1, "click", function (e) {
            //                        infoWindow.setContent(dataSrc.description);
            //                        infoWindow.open(map, marker1);
            //                    });
            //                })(marker1, dataSrc);
            //                //                (function (marker1, dataSrc) {

            //                //                }
            //            }




        }
    </script>
</head>
<body>
<div id="container" style="margin-top:100px; margin-left:150px;">
    <form id="form1" runat="server">
    <p>
 Nama Gudang : 
    <asp:label ID="lblNama" runat="server" Text="test" >
    </asp:label>
    </p>
    <p>
 Alamat : 
    <asp:label ID="lblAlamat" runat="server" Text="test" >
    </asp:label>
    </p>
            <table cellspacing="2">
            <tr>
            <td>
              
        <div id="map" style="width:400px; height:400px"></div>
   
    </td>
    <td>&nbsp;&nbsp;</td>
    <td>
    <div id="map2" style="width:400px; height:400px"></div>
   
    </td>
    <td valign="top">
    <asp:DataGrid ID="dgKunjungan1" class="table table-bordered" runat="server" 
                         AutoGenerateColumns="false" AllowPaging="False" PageSize="10" 
                         onitemcommand="dgKunjungan_ItemCommand" >
                         
                    <Columns >
                                        <asp:ButtonColumn DataTextField="Date" HeaderText="Tanggal" CommandName="id">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:ButtonColumn>
                                        <asp:BoundColumn DataField="Date" HeaderText="Tanggal" Visible=false>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Longitude" HeaderText="Longitude">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Latitude" HeaderText="Latitude">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                       
                                    </Columns>
                                     <PagerStyle CssClass="table_page" PrevPageText="previous ‹.." NextPageText="..› next" BackColor="#F7F7DE" ForeColor="Gray" HorizontalAlign="Right" Font-Bold="False" Font-Italic="False" Font-Names="verdana" Font-Overline="False" Font-Size="8pt" Font-Strikeout="False" Font-Underline="False" />
                    </asp:DataGrid>
    </td>
    </tr>
    <tr>
    <td></td>
    <td></td>
    <td></td>
    <td valign="top"> <p>Latitude: <input size="20" type="text" id="latbox" name="lat" runat="server" style="margin-left:10px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="latbox" ValidationGroup="valSettingKoordinatWarehousePop" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                     </p>
    <p>Longitude: <input size="20" type="text" id="lngbox" name="lng" runat="server" >
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="lngbox" ValidationGroup="valSettingKoordinatWarehousePop" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
    </p>
    <p>Radius: <input size="20" type="text" id="radius" name="rad" runat="server" style="margin-left:20px" placeholder="Fill with number...">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="radius" ValidationGroup="valSettingKoordinatWarehousePop" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
    </p></td>
            </tr>
           <tr>
            <td></td>
    <td></td>
    <td></td>
           <td> <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Save" ValidationGroup="valSettingKoordinatWarehousePop"/></td>
           </tr>
            </table>
           
   
 
<asp:TextBox ID="TextBox3" runat="server" style="visibility:hidden;"></asp:TextBox>
    </form>
 </div>
</body>
</html>
