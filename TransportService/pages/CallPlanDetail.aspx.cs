﻿/* documentation
 *001 17 Okt 2016 fernandes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;


public partial class CallPlanDetail : System.Web.UI.Page
{
    public string gRoleId, gUserId;

    public int PageNumber
    {
        get
        {
            if (ViewState["PageNumber"] != null)
                return Convert.ToInt32(ViewState["PageNumber"]);
            else
                return 0;
        }
        set
        {
            ViewState["PageNumber"] = value;
        }
    }

    public bool getRole()
    {
        //get user accesssrole
        gUserId = Session["User"].ToString();
        var vUser = UserFacade.GetUserbyID(gUserId);
        gRoleId = vUser.RoleID;
        string menuName = "Call Plan";
        var vRole = UserFacade.CheckUserLeverage(gRoleId,menuName);

        return vRole;
    }

    //001
    private void BindData(string keyword)
    {
        string CallPlanID = Request.QueryString["cid"];
        var listCPDetail = Core.Manager.CallPlanFacade.GetCallPlanDetail(CallPlanID,keyword);
        
        var role = getRole();
            foreach (var cpDetail in listCPDetail)
           {
               cpDetail.Role = role;
               btnAdd.Visible = role;
           }
        //RptCPDetaillist.DataSource = listCPDetail;
        //RptCPDetaillist.DataBind();

        PagedDataSource pgitems = new PagedDataSource();
        //DataView dv = new DataView(dt);
        pgitems.DataSource = listCPDetail;
        pgitems.AllowPaging = true;
        pgitems.PageSize = 10;
        pgitems.CurrentPageIndex = PageNumber;
        //lblPage.Text = pgitems.PageCount.ToString();
        if (pgitems.PageCount > 1)
        {
            lnkNext.Visible = true;
            lnkPrevious.Visible = true;
            rptPaging.Visible = true;

            ArrayList pages = new ArrayList();
            for (int i = 0; i < pgitems.PageCount; i++)
                pages.Add((i + 1).ToString());
            rptPaging.DataSource = pages;
            rptPaging.DataBind();

            Control control;
            control = rptPaging.Items[PageNumber].FindControl("btnPage");
            if (control != null)
            {
                ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
            }

            if (PageNumber == 0)
            {
                lnkPrevious.Enabled = false;
                lnkNext.Enabled = true;
            }
            else if (PageNumber == pgitems.PageCount - 1)
            {
                lnkPrevious.Enabled = true;
                lnkNext.Enabled = false;
            }
            else
            {
                lnkPrevious.Enabled = true;
                lnkNext.Enabled = true;
            }
        }
        else
        {
            rptPaging.Visible = false;
            lnkPrevious.Visible = false;
            lnkNext.Visible = false;
        }

        RptCPDetaillist.DataSource = pgitems;
        RptCPDetaillist.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //    if (Session["User"] == null)
            //    {
            //        Response.Redirect("home.aspx");
            //    }
            BindData(""); //001
        }
    }

    protected void rptCPDetail_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            Control control;
            control = e.Item.FindControl("lnkCPDetailId");

            if (control != null)
            {
                string id;
                id = ((Label)control).Text;

                CallPlanFacade.DeleteCPDetail(id);

                BindData(""); //001
                txtSearchCallPlan.Text = ""; //001
            }
        }

        if (e.CommandName == "Move")
        {
            Control control;
            control = e.Item.FindControl("lnkCPDetailId");

            if (control != null)
            {
                string id;
                id = ((Label)control).Text;

                Response.Redirect("UpdateCallPlanDetail.aspx?cid=" + id);

            }
        }

        if (e.CommandName == "Edit")
        {
            Control control;
            control = e.Item.FindControl("lnkCPDetailId");

            if (control != null)
            {
                string id;
                id = ((Label)control).Text;

                Response.Redirect("AddCallPlanDetail.aspx?cid=" + id);

            }
        }
        else
        {

        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string add = "add";
        string CallPlanID = Request.QueryString["cid"];
        Response.Redirect("AddCallPlanDetail.aspx?cadd=" + add + "&cid=" + CallPlanID); 
    }

    protected void btnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("CallPlan.aspx");
    }

    protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
        BindData(txtSearchCallPlan.Text); //001
    }

    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        PageNumber = PageNumber - 1;

        BindData(txtSearchCallPlan.Text); //001
    }

    protected void lnkNext_Click(object sender, EventArgs e)
    {
        PageNumber = PageNumber + 1;

        BindData(txtSearchCallPlan.Text); //001
    }

    //001
    //protected void RptCPDetaillist_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    Control control;
    //    control = e.Item.FindControl("RptDOlist");

    //    if (control != null)
    //    {
    //        Repeater deliveryOrder;
    //        deliveryOrder = ((Repeater)control);

    //        Control control2;
    //        control2 = e.Item.FindControl("CustomerId");

    //        string customerID = "";
    //        string warehouseID = "";
    //        if (control2 != null)
    //        {
    //            Control control3;
    //            control3 = e.Item.FindControl("WarehouseID");

    //            customerID = ((Label)control2).Text;
    //            warehouseID = ((Label)control3).Text;
    //        }
    //        deliveryOrder.DataSource = DeliveryOrderFacade.GetDOByCustomerID(customerID,warehouseID);
    //        deliveryOrder.DataBind();
    //    }
    //}

    //001
    protected void btnSearchCallPlan_Click(object sender, EventArgs e)
    {
        PageNumber = 0;
        BindData(txtSearchCallPlan.Text);
    }

}
//}