﻿/* documentation
 * 001 17 Okt 2016 fernandes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.Configuration;


namespace Core.Manager
{
    public class DeliveryOrderFacade : Base.Manager
    {
        //001
        //public static List<Model.DeliveryOrder> GetDOByCustomerID(string customerID,string warehouseID)
        //{
        //    var DO = DataContext.DeliveryOrders.Where(fld => fld.CustomerID.Equals(customerID) && fld.WarehouseID.Equals(warehouseID)).OrderByDescending(fld => fld.DODate).ToList();
        //    return DO;
        //}

        public static List<Model.mdlDeliveryOrder> LoadDeliveryOrder(Model.mdlParam json)
        {
            var mdlDeliveryOrderList = new List<Model.mdlDeliveryOrder>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@StartDate", SqlDbType = SqlDbType.DateTime, Value= DateTime.Now.Date },
                new SqlParameter() {ParameterName = "@FinishDate", SqlDbType = SqlDbType.DateTime, Value= DateTime.Now.Date.AddDays(1) },
                new SqlParameter() {ParameterName = "@EmployeeID", SqlDbType = SqlDbType.NVarChar, Value = json.EmployeeID },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = json.BranchID }
            };

            DataTable dtDeliveryOrder = DataFacade.DTSQLCommand(@"SELECT  a.DONumber
                    ,a.DODate
                    ,a.DOStatus
                    ,a.Description
                    ,a.CustomerID
                    ,a.EmployeeID
                    ,a.VehicleID 
                    ,a.BranchID
                    ,a.Signature
                    ,a.IsPrint
                    ,a.Remark 
                FROM DeliveryOrder a
                INNER JOIN (
		                    SELECT * FROM CallPlanDetail 
                            WHERE CallPlanID IN (
				                                 SELECT CallPlanID FROM CallPlan 
                                                 WHERE EmployeeID = @EmployeeID AND BranchID = @BranchID AND IsFinish = 0 AND Date >= @StartDate AND Date < @FinishDate
                                                )
		                   ) b ON b.CustomerID = a.CustomerID
                WHERE  EmployeeID = @EmployeeID AND BranchID = @BranchID  AND DODate >= @StartDate AND DODate < @FinishDate AND DOStatus = 'Shipper'", sp);
            //INNER JOIN  (select * from CallPlanDetail where CallPlanID='C1000001') b ON b.CustomerID = a.CustomerID


            foreach (DataRow row in dtDeliveryOrder.Rows)
            {
                var mdlDeliveryOrder = new Model.mdlDeliveryOrder();
                mdlDeliveryOrder.DONumber = row["DONumber"].ToString();
                mdlDeliveryOrder.CustomerID = row["CustomerID"].ToString();
                mdlDeliveryOrder.EmployeeID = row["EmployeeID"].ToString();
                mdlDeliveryOrder.VehicleID = row["VehicleID"].ToString(); //006--
                mdlDeliveryOrder.DODate = Convert.ToDateTime(row["DODate"]).ToString("yyyy-MM-dd hh:mm:ss");
                mdlDeliveryOrder.DOStatus = row["DOStatus"].ToString();
                mdlDeliveryOrder.Description = row["Description"].ToString();
                mdlDeliveryOrder.Signature = row["Signature"].ToString();
                mdlDeliveryOrder.IsPrint = row["IsPrint"].ToString();
                mdlDeliveryOrder.Remark = row["Remark"].ToString();
                mdlDeliveryOrder.BranchID = row["BranchID"].ToString();
                mdlDeliveryOrder.VisitID = "";
                mdlDeliveryOrderList.Add(mdlDeliveryOrder);
            }
            return mdlDeliveryOrderList;

        }

        public static List<Model.mdlDeliveryOrderDetail> LoadDeliveryOrderDetail(Model.mdlParam json, List<Model.mdlDeliveryOrder> listDO)
        {
            
            List<SqlParameter> sp = new List<SqlParameter>();
            StringBuilder sb = new StringBuilder();
            int count = 1;
            foreach(var DO in listDO)
            {
                
                var sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@DoNumbers" + count.ToString();
                if (DO == listDO.Last())
                {
                    sb.Append("@DoNumbers" + count.ToString());
                }
                else
                {
                    sb.Append("@DoNumbers" + count.ToString() + ",");
                }
                sqlParameter.SqlDbType = SqlDbType.NVarChar;
                sqlParameter.Value = DO.DONumber;
                sp.Add(sqlParameter);
                count++;
            }


            var mdlDeliveryOrderDetailList = new List<Model.mdlDeliveryOrderDetail>();
            var deliveryOrder = LoadDeliveryOrder(json);

            //List<SqlParameter> sp = new List<SqlParameter>()
            //{
            //    new SqlParameter() {ParameterName = "@StartDate", SqlDbType = SqlDbType.DateTime, Value= DateTime.Now.Date },
            //    new SqlParameter() {ParameterName = "@FinishDate", SqlDbType = SqlDbType.DateTime, Value= DateTime.Now.Date.AddDays(1) },
            //    new SqlParameter() {ParameterName = "@EmployeeID", SqlDbType = SqlDbType.NVarChar, Value = json.EmployeeID },
            //    new SqlParameter() {ParameterName = "@DoNumbers", SqlDbType = SqlDbType.NVarChar, Value =  DONumbers},
            //    new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = json.BranchID }
            //};
            

            
            

//            DataTable dtDeliveryOrderDetailList = DataFacade.DTSQLCommand(@"SELECT  DONumber,
//	                ProductID,
//	                UOM,
//	                Quantity,
//	                QuantityReal,
//	                ProductGroup,
//	                LotNumber,
//                    BoxID 
//                FROM DeliveryOrderDetail 
//                WHERE DONumber IN (
//				                SELECT a.DONumber 
//				                FROM DeliveryOrder a
//					                INNER JOIN (
//								                SELECT * FROM CallPlanDetail 
//								                WHERE CallPlanID IN (
//														                SELECT CallPlanID FROM CallPlan 
//														                WHERE EmployeeID = @EmployeeID AND BranchID = @BranchID AND IsFinish = 0 AND Date >= @StartDate
//														             )
//								                ) b ON b.CustomerID = a.CustomerID
//								                WHERE  EmployeeID = @EmployeeID AND BranchID = @BranchID  AND DODate >= @StartDate AND DOStatus = 'Shipper'
//				                    )", sp); 


            DataTable dtDeliveryOrderDetailList = DataFacade.DTSQLCommand(@"SELECT  DONumber,
	                ProductID,
                    UOM,
	                Quantity,
                    QuantityReal,
                    ProductGroup,
	                LotNumber,
                    BoxID 
               FROM DeliveryOrderDetail WHERE DONumber IN ("+sb.ToString()+")", sp);
            foreach (DataRow row in dtDeliveryOrderDetailList.Rows)
            {
                var mdlDeliveryOrderDetail = new Model.mdlDeliveryOrderDetail();
                mdlDeliveryOrderDetail.DONumber = row["DONumber"].ToString();
                mdlDeliveryOrderDetail.ProductID = row["ProductID"].ToString();
                mdlDeliveryOrderDetail.UOM = row["UOM"].ToString();
                mdlDeliveryOrderDetail.Quantity = row["Quantity"].ToString();
                mdlDeliveryOrderDetail.QuantityReal = row["QuantityReal"].ToString();
                mdlDeliveryOrderDetail.ProductGroup = row["ProductGroup"].ToString();
                mdlDeliveryOrderDetail.LotNumber = row["LotNumber"].ToString();
                mdlDeliveryOrderDetail.BoxID = row["BoxID"].ToString();
                mdlDeliveryOrderDetailList.Add(mdlDeliveryOrderDetail);
            }

            return mdlDeliveryOrderDetailList;
        }

        public static Model.mdlResultList UpdateDeliveryOrder(List<Model.mdlDeliveryOrderParam> lDOParamlist) //005,009 
        {
            var mdlResultList = new List<Model.mdlResult>();

            foreach (var lDOParam in lDOParamlist)
            {
                var mdlResult = new Model.mdlResult();
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@DONumber", SqlDbType = SqlDbType.NVarChar, Value = lDOParam.DONumber },
                    new SqlParameter() {ParameterName = "@DODate", SqlDbType = SqlDbType.DateTime, Value = lDOParam.DODate },
                    new SqlParameter() {ParameterName = "@DOStatus", SqlDbType = SqlDbType.NVarChar, Value = lDOParam.DOStatus },//
                    new SqlParameter() {ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = lDOParam.Description },
                    new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = lDOParam.CustomerID },
                    new SqlParameter() {ParameterName = "@EmployeeID", SqlDbType = SqlDbType.NVarChar, Value = lDOParam.EmployeeID },
                    new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lDOParam.BranchID },
                    new SqlParameter() {ParameterName = "@Signature", SqlDbType = SqlDbType.Text, Value = lDOParam.Signature },//
                    new SqlParameter() {ParameterName = "@IsPrint", SqlDbType = SqlDbType.NVarChar, Value = lDOParam.IsPrint },//
                    new SqlParameter() {ParameterName = "@Remark", SqlDbType = SqlDbType.VarChar, Value = lDOParam.Remark },
                    new SqlParameter() {ParameterName = "@LastDate", SqlDbType = SqlDbType.DateTime, Value = DateTime.Now },
                    new SqlParameter() {ParameterName = "@LastUpdateBy", SqlDbType = SqlDbType.NVarChar, Value = lDOParam.EmployeeID },
                    new SqlParameter() {ParameterName = "@VisitID", SqlDbType = SqlDbType.NVarChar, Value = lDOParam.VisitID }//
                };

                string query = @"UPDATE DeliveryOrder SET 
                                            DODate = @DODate, 
                                            DOStatus = @DOStatus, 
                                            Description = @Description, 
                                            CustomerID = @CustomerID,
                                            EmployeeID = @EmployeeID, 
                                            BranchID = @BranchID, 
                                            Signature = @Signature, 
                                            IsPrint = @IsPrint, 
                                            Remark = @Remark, 
                                            LastDate = @LastDate, 
                                            LastUpdateBy = @LastUpdateBy,
                                            VisitID = @VisitID    WHERE DONumber = @DONumber";
                mdlResult.Result = "|| " + "Update DO " + lDOParam.DONumber + " || " + Manager.DataFacade.DTSQLVoidCommand(query, sp); 

              
                if (mdlResult.Result.Contains("SQLSuccess") == true) 
                {
                }
                else
                {
                    string ResultSubstring;

                   
                    if (mdlResult.Result.Length > 500)
                    {
                        ResultSubstring = mdlResult.Result.Substring(0, 500);

                        mdlResult.Result = ResultSubstring;
                    }
                 

                }
                //014--

                mdlResultList.Add(mdlResult);
            }

            var mdlResultListnew = new Model.mdlResultList(); 
            mdlResultListnew.ResultList = mdlResultList; 

            return mdlResultListnew;
        }

        public static Model.mdlResultList UpdateDeliveryOrderDetail(List<Model.mdlDeliveryOrderDetailParam> lDODetailParamlist) 
        {
            int lResendTimes = Convert.ToInt32(ConfigurationManager.AppSettings["ResendTimes"]);
            string lAutoInv = ConfigurationManager.AppSettings["autoInv"];     
            string lAutoPost = ConfigurationManager.AppSettings["autoPost"];   
            var mdlResultList = new List<Model.mdlResult>();
            string lDoNumber = string.Empty;


            foreach (var lDODetailParam in lDODetailParamlist)
            {
                var mdlResult = new Model.mdlResult();
                lDoNumber = lDODetailParam.DONumber;

                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@DONumber", SqlDbType = SqlDbType.NVarChar, Value = lDODetailParam.DONumber },
                    new SqlParameter() {ParameterName = "@ProductID", SqlDbType = SqlDbType.NVarChar, Value = lDODetailParam.ProductID },
                    new SqlParameter() {ParameterName = "@UOM", SqlDbType = SqlDbType.NVarChar, Value = lDODetailParam.UOM },
                    new SqlParameter() {ParameterName = "@Quantity", SqlDbType = SqlDbType.Int, Value = lDODetailParam.Quantity },
                    new SqlParameter() {ParameterName = "@QuantityReal", SqlDbType = SqlDbType.Int, Value = lDODetailParam.QuantityReal },
                    new SqlParameter() {ParameterName = "@ProductGroup", SqlDbType = SqlDbType.NVarChar, Value = lDODetailParam.ProductGroup },
                    new SqlParameter() {ParameterName = "@LotNumber", SqlDbType = SqlDbType.NVarChar, Value = lDODetailParam.LotNumber },
                    new SqlParameter() {ParameterName = "@ReasonID", SqlDbType = SqlDbType.NVarChar, Value = lDODetailParam.ReasonID },
                    new SqlParameter() {ParameterName = "@BoxID", SqlDbType = SqlDbType.NVarChar, Value = lDODetailParam.BoxID }
                };

                string query = "UPDATE DeliveryOrderDetail SET UOM = @UOM, Quantity = @Quantity, QuantityReal = @QuantityReal, ProductGroup = @ProductGroup, LotNumber = @LotNumber, ReasonID = @ReasonID, BoxID = @BoxID WHERE DONumber = @DONumber and ProductID = @ProductID";
                mdlResult.Result = "|| " + "Update DODetail " + lDODetailParam.DONumber + " || " + Manager.DataFacade.DTSQLVoidCommand(query, sp);

       
                if (mdlResult.Result.Contains("SQLSuccess") == true) 
                {
                }
                else
                {
                    string ResultSubstring;

                   
                    if (mdlResult.Result.Length > 500)
                    {
                        ResultSubstring = mdlResult.Result.Substring(0, 500);

                        mdlResult.Result = ResultSubstring;
                    }
                  

                }
         

                mdlResultList.Add(mdlResult);
            }

            var mdlResultListnew = new Model.mdlResultList();
            mdlResultListnew.ResultList = mdlResultList;           

            return mdlResultListnew;
            
        }

        
              
        public static Model.mdlDeliveryOrder LoadDeliveryOrderbyDONumber(string lDoNumber)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@DoNumber", SqlDbType = SqlDbType.NVarChar, Value= lDoNumber }
            };

            DataTable dtDeliveryOrder = DataFacade.DTSQLCommand(@"SELECT TOP 1 DONumber
                                                                            ,DODate
                                                                            ,BranchID
                                                                            ,VehicleID
                                                                    From DeliveryOrder WHERE DONumber = @DoNumber", sp);
            var mdlDeliveryOrder = new Model.mdlDeliveryOrder();
            foreach (DataRow row in dtDeliveryOrder.Rows)
            {

                mdlDeliveryOrder.DONumber = row["DONumber"].ToString();
                mdlDeliveryOrder.DODate = Convert.ToDateTime(row["DODate"]).ToString("yyyy-MM-dd hh:mm:ss");
                mdlDeliveryOrder.BranchID = row["BranchID"].ToString();
                mdlDeliveryOrder.VehicleID = row["VehicleID"].ToString();
            }

            return mdlDeliveryOrder;
        }
       
        //001
        public static List<Model.mdlDOReport> LoadDeliveryOrderReport(string lBranchID, DateTime lDODate, DateTime lDOEndDate, List<string> lEmployeeIDlist)
        {
            string lParam = string.Empty;

            foreach (var lEmployeeID in lEmployeeIDlist)
            {
                if (lParam == "")
                {
                    lParam = " a.EmployeeID =" + "'" + lEmployeeID + "'"; 
                }
                else
                {
                    lParam += " OR a.EmployeeID =" + "'" + lEmployeeID + "'";
                }
            }

            lParam = "(" + lParam + ")";

            List<SqlParameter> sp = new List<SqlParameter>()
            {
               new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = "%"+lBranchID+"%" },
               new SqlParameter() {ParameterName = "@DODate", SqlDbType = SqlDbType.Date, Value = lDODate },
               new SqlParameter() {ParameterName = "@DOEndDate", SqlDbType = SqlDbType.Date, Value = lDOEndDate.AddDays(1) }, 
              
            };

            var mdlDOReportList = new List<Model.mdlDOReport>();

            DataTable dtDeliveryOrderReport = Manager.DataFacade.DTSQLCommand(@"SELECT   a.BranchID, 
			                                                                        c.BranchName,
                                                                                    a.EmployeeID,
                                                                                    g.EmployeeName,
                                                                                    f.VehicleID,    
			                                                                        a.CustomerID,
			                                                                        e.CustomerName,
                                                                                    a.WarehouseID,
                                                                                    i.WarehouseName, 
			                                                                        a.DONumber,
                                                                                    a.Remark,
                                                                                    a.DODate,
																					(select top 1 ImageBase64 from CustomerImage where DocNumber=a.DONumber) imagebase64,
                                                                                    (select top 1 ImagePath from CustomerImage where DocNumber=a.DONumber) imagepath,    
			                                                                        b.ProductID,
                                                                                    d.ArticleNumber, 
			                                                                        d.ProductName,
                                                                                    b.LotNumber,
			                                                                        b.Quantity,
                                                                                    b.UOM,
			                                                                        b.QuantityReal,
                                                                                    h.Value
			                                                                       
				                                                            FROM DeliveryOrder a
				                                                            INNER JOIN DeliveryOrderDetail b ON b.DONumber = a.DONumber
				                                                            INNER JOIN Branch c ON c.BranchID = a.BranchID 
				                                                            INNER JOIN Customer e ON e.CustomerID = a.CustomerID
				                                                            INNER JOIN Product d ON d.ProductID = b.ProductID
                                                                            INNER JOIN Employee g ON g.EmployeeID = a.EmployeeID
                                                                            LEFT JOIN Reason h ON h.ReasonID = b.ReasonID
                                                                            LEFT JOIN Visit f ON f.VisitID = a.VisitID
                                                                            LEFT JOIN Warehouse i ON i.WarehouseID = a.WarehouseID
				                                                            Where (a.BranchID LIKE @BranchID) AND (a.DODate between @DODate and @DOEndDate) AND " + lParam, sp); //018

            foreach (DataRow row in dtDeliveryOrderReport.Rows)
            {
                var mdlDOReport = new Model.mdlDOReport();
                mdlDOReport.BranchID = row["BranchID"].ToString();
                mdlDOReport.BranchName = row["BranchName"].ToString();
                mdlDOReport.CustomerID = row["CustomerID"].ToString();
                mdlDOReport.CustomerName = row["CustomerName"].ToString();
                mdlDOReport.WarehouseID = row["WarehouseID"].ToString();
                mdlDOReport.WarehouseName = row["WarehouseName"].ToString();
                mdlDOReport.DONumber = row["DONumber"].ToString();
                mdlDOReport.ProductID = row["ProductID"].ToString();
                mdlDOReport.ArticleNumber = row["ArticleNumber"].ToString();
                mdlDOReport.ProductName = row["ProductName"].ToString();
                mdlDOReport.LotNumber = row["LotNumber"].ToString();
                mdlDOReport.Quantity = Convert.ToInt32(row["Quantity"]);
                mdlDOReport.UOM = row["UOM"].ToString();
                mdlDOReport.QuantityReal = Convert.ToInt32(row["QuantityReal"]);
                mdlDOReport.ImageBase64 = row["ImageBase64"].ToString();
                mdlDOReport.ImagePath = row["imagepath"].ToString();
                mdlDOReport.DODate = Convert.ToDateTime(row["DODate"]).ToString();
                mdlDOReport.EmployeeID = row["EmployeeID"].ToString();
                mdlDOReport.EmployeeName = row["EmployeeName"].ToString();
                mdlDOReport.VehicleID = row["VehicleID"].ToString();
                mdlDOReport.Remark = row["Remark"].ToString(); 
                mdlDOReport.Reason = row["Value"].ToString(); 

                mdlDOReportList.Add(mdlDOReport);
            }
            return mdlDOReportList;
        }

    }
}
