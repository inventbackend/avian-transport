﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
//using License;
using InventLicense;
using System.Collections;
using System.Globalization;

namespace TransportService.pages
{
    public partial class UserConfig : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId, gRoleId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "User Config";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        private void ClearFormUserConfigContent()
        {
            if (getBranch() == "")
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
                //txtBranchID.Text = string.Empty;
                //txtBranchID.ReadOnly = false;
            }
            else
            {
                ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                ddlSearchBranchID.DataTextField = "BranchName";
                ddlSearchBranchID.DataValueField = "BranchID";
                ddlSearchBranchID.DataBind();
                //txtBranchID.Text = getBranch();
                //txtBranchID.ReadOnly = true;
            }

            txtDeviceID.ReadOnly = false;
            txtDeviceID.Text = string.Empty;
            //txtBranchID.Text = string.Empty;
            txtEmployee.Text = string.Empty;
            txtVehicleNumber.Text = string.Empty;
            txtIpLocal.Text = string.Empty;
            txtPortLocal2.Value = string.Empty;
            txtIpPublic.Text = string.Empty;
            txtPortPublic2.Value = string.Empty;
            txtIpAlternative.Text = string.Empty;
            txtPortAlternative2.Value = string.Empty;
            txtPassword.Text = string.Empty;
        }

        private void ClearContent()
        {
            txtDeviceID.Text = string.Empty;
            //txtBranchID.Text = string.Empty;
            txtEmployee.Text = string.Empty;
            txtVehicleNumber.Text = string.Empty;
            txtIpLocal.Text = string.Empty;
            txtPortLocal2.Value = string.Empty;
            txtIpPublic.Text = string.Empty;
            txtPortPublic2.Value = string.Empty;
            txtIpAlternative.Text = string.Empty;
            txtPortAlternative2.Value = string.Empty;
            txtSearchUserConfig.Text = "";
            txtPassword.Text = "";

            //license information -- FERNANDES
            mdlCustomerLicense mdlCustomerLicense = CustomerFacade.LoadCustomerLicenseKey("AVIAN");
            string LicenseKey = mdlCustomerLicense.LicenseKey;

            NumberFormatInfo nfi = new CultureInfo("id-ID", false).NumberFormat;

            string license = LicenseFacade.GetLicenseQty(LicenseKey, "AVIAN").ToString("N", nfi);
            string licenseRegistered = UserConfigFacade.LoadUserConfig("", "").Count.ToString("N", nfi);
            string licenseAvailable = (LicenseFacade.GetLicenseQty(LicenseKey, "AVIAN") - UserConfigFacade.LoadUserConfig("", "").Count).ToString("N", nfi);

            lblLicense.Text = license.Substring(0, license.IndexOf(","));
            lblLicenseRegistered.Text = licenseRegistered.Substring(0, licenseRegistered.IndexOf(","));
            lblLicenseAvailable.Text = licenseAvailable.Substring(0, licenseAvailable.IndexOf(","));
            //--

            BindData("",getBranch());
            PanelFormUserConfig.Visible = false;
        }

        private void BindData(string lkeyword, string branchid)
        {
            var listUserConfig = new List<Core.Model.mdlUserConfig>();

            if (getBranch() == "")
            {
                listUserConfig = UserConfigFacade.LoadUserConfig(lkeyword, branchid);
            }
            else
            {
                listUserConfig = UserConfigFacade.LoadUserConfigByCertainBranch(lkeyword, branchid);
            }

            var role = getRole();
            foreach (var UserConfig in listUserConfig)
            {
                UserConfig.Role = role;
                btnNew.Visible = role;
            }

            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = listUserConfig;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;

            if (pgitems.PageCount > 1)
            {
                lnkNext.Visible = true;
                lnkPrevious.Visible = true;
                rptPaging.Visible = true;

                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }


            RptUserConfiglist.DataSource = pgitems;
            RptUserConfiglist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearContent();
            }
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindData(txtSearchUserConfig.Text, getBranch());
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            BindData(txtSearchUserConfig.Text, getBranch());
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            BindData(txtSearchUserConfig.Text, getBranch());
        }

        protected void rptUserConfig_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Link")
            {
                Control control;
                control = e.Item.FindControl("lnkDeviceId");
                if (control != null)
                {
                    string id;
                    id = ((LinkButton)control).Text;

                    var lUserConfigs = UserConfigFacade.LoadUserConfigbyDeviceID(id);
                    txtDeviceID.Text = lUserConfigs.DeviceID;
                    
                    //set value of ddlbranchid
                    if (getBranch() == "")
                    {
                        ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                        ddlSearchBranchID.DataTextField = "BranchName";
                        ddlSearchBranchID.DataValueField = "BranchID";
                        ddlSearchBranchID.DataBind();
                    }
                    else
                    {
                        ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                        ddlSearchBranchID.DataTextField = "BranchName";
                        ddlSearchBranchID.DataValueField = "BranchID";
                        ddlSearchBranchID.DataBind();
                    }
                    //--
                    ddlSearchBranchID.SelectedValue = lUserConfigs.BranchID;
                    txtEmployee.Text = lUserConfigs.EmployeeID;
                    txtVehicleNumber.Text = lUserConfigs.VehicleNumber;
                    txtIpLocal.Text = lUserConfigs.IpLocal;
                    txtPortLocal2.Value = lUserConfigs.PortLocal;
                    txtIpPublic.Text = lUserConfigs.IpPublic;
                    txtPortPublic2.Value = lUserConfigs.PortPublic;
                    txtIpAlternative.Text = lUserConfigs.IpAlternative;
                    txtPortAlternative2.Value = lUserConfigs.PortAlternative;
                    txtPassword.Text = lUserConfigs.Password;

                    btnInsert.Visible = false;
                    btnUpdate.Visible = true;
                    txtDeviceID.ReadOnly = true;

                    PanelFormUserConfig.Visible = true;

                }
            }

            if (e.CommandName == "Delete")
            {
                Control controlDeviceID,controlEmployeeID;

                controlDeviceID = e.Item.FindControl("lnkDeviceId");
                controlEmployeeID = e.Item.FindControl("lblEmployeeId");

                if (controlDeviceID != null && controlEmployeeID != null)
                {
                    string Deviceid,Employeeid;

                    Deviceid = ((LinkButton)controlDeviceID).Text;
                    Employeeid = ((Label)controlEmployeeID).Text;

                    //string lResult = UserConfigFacade.DeleteUserConfig(Deviceid,Employeeid);

                    UserConfigFacade.DeleteUserConfig(Deviceid, Employeeid);

                    //if (lResult == "1")
                    //{
                    //    Response.Write("<script>alert('Delete User Config Success');</script>");
                    //}
                    //else
                    //{
                    //    Response.Write("<script>alert('Delete User Config Failed');</script>");
                    //}

                    BindData(txtSearchUserConfig.Text, getBranch());
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var branchdetail = BranchFacade.GetBranchbyID(ddlSearchBranchID.SelectedValue);

            string Employee = txtEmployee.Text;
            string[] splitEmployee = Employee.Split('-');
            string lEmployeeID = splitEmployee.FirstOrDefault().Trim();

            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string user = Session["User"].ToString();

            bool lCheckEmployee = EmployeeFacade.CheckExistingEmployee(ddlSearchBranchID.SelectedValue, lEmployeeID);
            if (lCheckEmployee == true)
            {
                Response.Write("<script>alert('The Employee Code is not Exist, Please Contact Admin');</script>");

                return;
            }

            string lResult = UserConfigFacade.UpdateUserConfig(txtDeviceID.Text, ddlSearchBranchID.SelectedValue, branchdetail.BranchName, lEmployeeID, txtVehicleNumber.Text, txtIpLocal.Text, txtPortLocal2.Value, txtIpPublic.Text, txtPortPublic2.Value, txtIpAlternative.Text, txtPortAlternative2.Value,txtPassword.Text,now,user);

            if (lResult == "1")
            {
                Response.Write("<script>alert('Update User Config Success');</script>");
            }
            else
            {
                Response.Write("<script>alert('Update User Config Failed');</script>");
            }

            ClearContent();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            var branchdetail = BranchFacade.GetBranchbyID(ddlSearchBranchID.SelectedValue);

            string Employee = txtEmployee.Text;
            string[] splitEmployee = Employee.Split('-');
            string lEmployeeID = splitEmployee.FirstOrDefault().Trim();

            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string user = Session["User"].ToString();

            bool lCheckEmployee = EmployeeFacade.CheckExistingEmployee(ddlSearchBranchID.SelectedValue,lEmployeeID);
            if (lCheckEmployee == true)
            {
                Response.Write("<script>alert('The Employee Code is not Exist, Please Contact Admin');</script>");

                return;
            }

            string lResult = UserConfigFacade.InsertUserConfig(txtDeviceID.Text, ddlSearchBranchID.SelectedValue, branchdetail.BranchName, lEmployeeID, txtVehicleNumber.Text, txtIpLocal.Text, txtPortLocal2.Value, txtIpPublic.Text, txtPortPublic2.Value, txtIpAlternative.Text, txtPortAlternative2.Value, txtPassword.Text, now, user);

            if (lResult == "1")
            {
                Response.Write("<script>alert('Insert User Config Success');</script>");
            }
            else
            {
                Response.Write("<script>alert('Insert User Config Fail');</script>");
            }

            ClearContent();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFormUserConfigContent();

            PanelFormUserConfig.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            //call the webservice to check the license limit -- FERNANDES
            //var paramCompany = new mdlCompanyParam();
            //paramCompany.CompanyName = "AVIAN";

            //string uri = "";
            //string json = Core.Services.RestPublisher.Serialize(paramCompany);

            //var resultLimit = WebServiceFacade.getLicenseLimit(uri,json);

            //string s = resultLimit.Limit; 
            //end

            //getLicenseLimit -- FERNANDES
            mdlCustomerLicense mdlCustomerLicense = CustomerFacade.LoadCustomerLicenseKey("AVIAN");
            string LicenseKey = mdlCustomerLicense.LicenseKey;

            int licenseLimitQty;
            try
            {
                licenseLimitQty = LicenseFacade.GetLicenseQty(LicenseKey,"AVIAN");
            }
            catch
            {
                licenseLimitQty = 0;
            }

            //check licenseLimit
            var UserConfigData = UserConfigFacade.LoadUserConfig("", "");

            if (UserConfigData.Count >= licenseLimitQty)
            {
                Response.Write("<script>alert('License Limit Exceeded, Please Contact Admin');</script>");

                return;
            }

            PanelFormUserConfig.Visible = true;
            btnUpdate.Visible = false;
            btnInsert.Visible = true;

            ClearFormUserConfigContent();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

        }

        protected void btnSearchUserConfig_Click(object sender, EventArgs e)
        {
            PageNumber = 0;
            BindData(txtSearchUserConfig.Text, getBranch());
            PanelFormUserConfig.Visible = false;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBranchID(string prefixText, int count)
        {
            List<string> lBranchs = new List<string>();
            lBranchs = BranchFacade.AutoComplBranch(prefixText, count, "BranchID");
            return lBranchs;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchEmployee(string prefixText, int count, string contextKey)
        {
            List<string> lEmployees = new List<string>();
            lEmployees = EmployeeFacade.AutoComplEmployeeUserConf(prefixText, count, "EmployeeID", contextKey);
            return lEmployees;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchVehicleNumber(string prefixText, int count)
        {
            List<string> lVehicleNumbers = new List<string>();
            lVehicleNumbers = VehicleFacade.AutoComplVehicleNumberUserConf(prefixText, count, "VehicleNumber");
            return lVehicleNumbers;
        }

    }
}