﻿/* documentation
 * 001 20 Okt'16 Fernandes
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Core.Manager;

namespace TransportService.pages
{
    public partial class ReportRetur : System.Web.UI.Page
    {
        private void ClearContent()
        {
            txtBranchID.Text = string.Empty;
            Panel1.Visible = false;
            btnSelectAll.Visible = false;
            btnUnSelectAll.Visible = false;
            txtdate1.Text = string.Empty;
            txtdate2.Text = string.Empty;
            PanelReportRetur.Visible = false;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PanelReportRetur.Visible = false;
                ClearContent();
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in blEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }

    
            if (blEmployeeStrList.Count == 0)
            {
                lblValblEmployee.Text = "*Required";
                lblValblEmployee.Visible = true;
                PanelReportRetur.Visible = false;

                return;
            }
            else
            {
                lblValblEmployee.Visible = false;
            }


            var ds = ReturFacade.LoadReturReport(txtBranchID.Text, Convert.ToDateTime(txtdate1.Text), Convert.ToDateTime(txtdate2.Text), blEmployeeStrList);
            ReportDataSource rds = new ReportDataSource("dsRetur", ds);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportRetur.rdlc");

            ReportParameter[] parameter = new ReportParameter[2];
            parameter[0] = new ReportParameter("StartDate", txtdate1.Text);
            parameter[1] = new ReportParameter("EndDate", txtdate2.Text);
            ReportViewer1.LocalReport.SetParameters(parameter);

            ReportViewer1.LocalReport.Refresh();

            PanelReportRetur.Visible = true;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBranchID(string prefixText, int count)
        {
            List<string> lBranchs = new List<string>();
            lBranchs = BranchFacade.AutoComplBranch(prefixText, count, "BranchID");
            return lBranchs;
        }

        protected void btnShowEmployee_Click(object sender, EventArgs e)
        {

            if (txtEmployeeID.Text == "")
            {
                var lEmployeelist = EmployeeFacade.LoadEmployeelistReport(txtBranchID.Text);
                blEmployee.DataSource = lEmployeelist;
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();
            }
            else
            {
                var lEmployeelist = EmployeeFacade.LoadEmployeelistReport2(txtEmployeeID.Text, txtBranchID.Text); //001
                blEmployee.DataSource = lEmployeelist;
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();
            }

            Panel1.Visible = true;
            btnSelectAll.Visible = true;
            btnUnSelectAll.Visible = true;
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = true;
            }
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = false;
            }
        }

    }
}