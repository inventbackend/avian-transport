﻿/* documentation
 *001 - 14 Okt'16 - fernandes
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Drawing;

namespace TransportService.pages
{
    public partial class SettingKoordinatPop : System.Web.UI.Page
    {
        //generate color array
        static string[] ColourValues = new string[] {
        "800000", "008000", "000080", "808000", "800080", "008080", "808080",
        "FF0000", "00FF00", "0000FF", "FFFF00", "FF00FF", "00FFFF", "000000",
        "C00000", "00C000", "0000C0", "C0C000", "C000C0", "00C0C0", "C0C0C0", 
        "400000", "004000", "000040", "404000", "400040", "004040", "404040", 
        "200000", "002000", "000020", "202000", "200020", "002020", "202020", 
        "600000", "006000", "000060", "606000", "600060", "006060", "606060", 
        "A00000", "00A000", "0000A0", "A0A000", "A000A0", "00A0A0", "A0A0A0", 
        "E00000", "00E000", "0000E0", "E0E000", "E000E0", "00E0E0", "E0E0E0",
        "800000", "008000", "000080", "808000", "800080", "008080", "808080",
        "FF0000", "00FF00", "0000FF", "FFFF00", "FF00FF", "00FFFF", "000000",
        "C00000", "00C000", "0000C0", "C0C000", "C000C0", "00C0C0", "C0C0C0", 
        "400000", "004000", "000040", "404000", "400040", "004040", "404040", 
        "200000", "002000", "000020", "202000", "200020", "002020", "202020", 
        "600000", "006000", "000060", "606000", "600060", "006060", "606060", 
        "A00000", "00A000", "0000A0", "A0A000", "A000A0", "00A0A0", "A0A0A0", 
        "E00000", "00E000", "0000E0", "E0E000", "E000E0", "00E0E0", "E0E0E0",
        };

        private void bindRBLColor()
        {
            int n = 0;
            foreach (ListItem i in rblCustomer.Items)
            {
                i.Attributes["style"] = "color:#" + ColourValues[n] + ";";

                n++;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                var txtSession = Session["txtCustomerId"];

                if(txtSession == null || Convert.ToString(txtSession) == "")
                {
                    Response.Redirect("SettingKoordinat.aspx");
                }
                else
                {

                    string txtCustomerId = Session["txtCustomerId"].ToString();
                    string cldF = Session["CldFrom"].ToString();
                    string cldT = Session["CldTo"].ToString();
                    DateTime cldFrom = Convert.ToDateTime(cldF);
                    DateTime cldTo = Convert.ToDateTime(cldT);

                    var listCustomer = new List<Core.Model.Customer>();
                    var customer = CustomerFacade.GetCustomerDetail(txtCustomerId);
                    if (customer == null)
                    {

                    }
                    else
                    {
                        lblNama.Text = customer.CustomerName;
                        //lblNama.Text = customer.CustomerID;
                        //lblKeterangan.Text = customer.CustomerID;
                        lblAlamat.Text = customer.CustomerAddress;
                        listCustomer.Add(customer);

                        rptVisit.DataSource = listCustomer;
                        rptVisit.DataBind();

                        var listKoordinat = VisitFacade.GetKoordinatKunjungan(cldFrom, cldTo, txtCustomerId);

                        var koordinatMaster = new Core.Model.mdlKoordinatKunjungan();
                        koordinatMaster.Date = "Master";
                        koordinatMaster.Longitude = customer.Longitude;
                        koordinatMaster.Latitude = customer.Latitude;

                        listKoordinat.Insert(0, koordinatMaster);

                        dgKunjungan1.DataSource = listKoordinat;
                        dgKunjungan1.DataBind();

                        if (listKoordinat.Count > 1)
                        {
                            rptMarkers.DataSource = VisitFacade.GetKoordinatKunjungan(cldFrom, cldTo, txtCustomerId);
                            rptMarkers.DataBind();
                        }
                        else
                        {
                            rptMarkers.DataSource = listKoordinat;
                            rptMarkers.DataBind();
                        }
                    }

                    var CustomerDetail = CustomerFacade.GetCustomerDetail(txtCustomerId); //001
                    radius.Value = Convert.ToString(CustomerDetail.Radius); //001

                    var lCustomerlist = CustomerFacade.LoadCustomernWarehouse(txtCustomerId);
                    //dgToko.DataSource = lCustomerlist;
                    //dgToko.DataBind();
                    rblCustomer.DataSource = lCustomerlist;
                    rblCustomer.DataTextField = "CustomerName";
                    rblCustomer.DataValueField = "CustomerID";
                    rblCustomer.DataBind();

                    rblCustomer.SelectedIndex = 0;
                }
            }

            //get the last zoom position
            zoom_data_map.Value = zoom_data_map.Value;
            zoom_data_map2.Value = zoom_data_map2.Value;

            //get the last latlong position in the right side map
            templatbox.Value = templatbox.Value;
            templngbox.Value = templngbox.Value;

            //get the last latlong position in the left side map
            templatbox2.Value = templatbox2.Value;
            templngbox2.Value = templngbox2.Value;

            var txtSessionPostback = Session["txtCustomerId"];

            if (txtSessionPostback == null || Convert.ToString(txtSessionPostback) == "")
            {
                Response.Redirect("SettingKoordinat.aspx");
            }

            bindRBLColor();
            Page.MaintainScrollPositionOnPostBack = true;
        }

        protected void dgKunjungan_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "id")
            {
                //bindRBLColor();

                string txtCustomerId = Session["txtCustomerId"].ToString();
                var listCustomer = new List<Core.Model.Customer>();
                var customer = CustomerFacade.GetCustomerDetail(txtCustomerId);

                customer.Longitude = e.Item.Cells[2].Text; // kolom/sel ke 2 di tabel yg dipilih
                customer.Latitude = e.Item.Cells[3].Text;
                listCustomer.Add(customer);

                rptVisit.DataSource = listCustomer;
                rptVisit.DataBind();

                lngbox.Value = e.Item.Cells[2].Text;
                latbox.Value = e.Item.Cells[3].Text;
                TextBox3.Text = e.Item.Cells[1].Text;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //bindRBLColor(); 

            lblNama.Text = rblCustomer.SelectedValue; //FERNANDES

            string txtCustomerId = Session["txtCustomerId"].ToString();
            string cldF = Session["CldFrom"].ToString();
            string cldT = Session["CldTo"].ToString();
            DateTime cldFrom = Convert.ToDateTime(cldF);
            DateTime cldTo = Convert.ToDateTime(cldT);

            var lCustomerlist = CustomerFacade.LoadCustomernWarehouseAddr(lblNama.Text, txtCustomerId);
            foreach (var lcustomer in lCustomerlist)
            {
                if (lcustomer.CustomerType == "customer")
                {
                    Core.Manager.CustomerFacade.UpdateLongLatCustomer(txtCustomerId, lngbox.Value, latbox.Value, Convert.ToDecimal(radius.Value)); //001

                    //Response.Redirect("SettingKoordinatPop.aspx");
                    var listCustomer = new List<Core.Model.Customer>();
                    var customer = CustomerFacade.GetCustomerDetail(txtCustomerId);
                    if (customer == null)
                    {

                    }
                    else
                    {
                        listCustomer.Add(customer);
                        rptVisit.DataSource = listCustomer;
                        rptVisit.DataBind();

                        var listKoordinat = VisitFacade.GetKoordinatKunjungan(cldFrom, cldTo, txtCustomerId);

                        var koordinatMaster = new Core.Model.mdlKoordinatKunjungan();
                        koordinatMaster.Date = "Master";
                        koordinatMaster.Longitude = customer.Longitude;
                        koordinatMaster.Latitude = customer.Latitude;
                        listKoordinat.Insert(0, koordinatMaster);

                        dgKunjungan1.DataSource = listKoordinat;
                        dgKunjungan1.DataBind();


                        rptMarkers.DataSource = VisitFacade.GetKoordinatKunjungan(cldFrom, cldTo, txtCustomerId);
                        rptMarkers.DataBind();
                    }
                }
                else
                {
                    Core.Manager.WarehouseFacade.UpdateLongLatWarehouse(lblNama.Text, lngbox.Value, latbox.Value, Convert.ToDecimal(radius.Value), txtCustomerId);

                    //Response.Redirect("SettingKoordinatPop.aspx");
                    var listWarehouse = new List<Core.Model.Warehouse>();
                    var warehouse = WarehouseFacade.GetWarehouseDetail(lblNama.Text,txtCustomerId);
                    if (warehouse == null)
                    {

                    }
                    else
                    {
                        listWarehouse.Add(warehouse);
                        rptVisit.DataSource = listWarehouse;
                        rptVisit.DataBind();

                        var listKoordinat = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, lblNama.Text ,txtCustomerId);

                        var koordinatMaster = new Core.Model.mdlKoordinatKunjunganGudang();
                        koordinatMaster.Date = "Master";
                        koordinatMaster.Longitude = warehouse.Longitude;
                        koordinatMaster.Latitude = warehouse.Latitude;
                        listKoordinat.Insert(0, koordinatMaster);

                        dgKunjungan1.DataSource = listKoordinat;
                        dgKunjungan1.DataBind();


                        rptMarkers.DataSource = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo,lblNama.Text, txtCustomerId);
                        rptMarkers.DataBind();
                    }
                }
            }
        }

        protected void rblCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bindRBLColor();
            //dgKunjungan1.CurrentPageIndex = 0;

            lblNama.Text = rblCustomer.SelectedValue;
            string customerID = Session["txtCustomerId"].ToString();
            string cldF = Session["CldFrom"].ToString();
            string cldT = Session["CldTo"].ToString();
            DateTime cldFrom = Convert.ToDateTime(cldF);
            DateTime cldTo = Convert.ToDateTime(cldT);

            var lCustomerlist = CustomerFacade.LoadCustomernWarehouseAddr(lblNama.Text, customerID);
            foreach (var lcustomer in lCustomerlist)
            {
                lblAlamat.Text = lcustomer.CustomerAddress;

                if (lcustomer.CustomerType == "customer")
                {
                    var listCustomer = new List<Core.Model.Customer>();
                    var customer = CustomerFacade.GetCustomerDetail(lblNama.Text);
                    if (customer == null)
                    {

                    }
                    else
                    {
                        listCustomer.Add(customer);

                        rptVisit.DataSource = listCustomer;
                        rptVisit.DataBind();

                        var listKoordinat = VisitFacade.GetKoordinatKunjungan(cldFrom, cldTo, lblNama.Text);

                        var koordinatMaster = new Core.Model.mdlKoordinatKunjungan();
                        koordinatMaster.Date = "Master";
                        koordinatMaster.Longitude = customer.Longitude;
                        koordinatMaster.Latitude = customer.Latitude;

                        listKoordinat.Insert(0, koordinatMaster);

                        dgKunjungan1.DataSource = listKoordinat;
                        dgKunjungan1.DataBind();

                        if (listKoordinat.Count > 1)
                        {
                            rptMarkers.DataSource = VisitFacade.GetKoordinatKunjungan(cldFrom, cldTo, lblNama.Text);
                            rptMarkers.DataBind();
                        }
                        else
                        {
                            rptMarkers.DataSource = listKoordinat;
                            rptMarkers.DataBind();
                        }
                    }

                    radius.Value = Convert.ToString(customer.Radius); //001
                }
                else
                {
                    var listWarehouse = new List<Core.Model.Warehouse>();
                    var warehouse = WarehouseFacade.GetWarehouseDetail(lblNama.Text, customerID);
                    if (warehouse == null)
                    {

                    }
                    else
                    {
                        listWarehouse.Add(warehouse);

                        rptVisit.DataSource = listWarehouse;
                        rptVisit.DataBind();

                        string warehouseCustomer = warehouse.CustomerID;

                        var listKoordinat = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, lblNama.Text, warehouseCustomer);

                        var koordinatMaster = new Core.Model.mdlKoordinatKunjunganGudang();
                        koordinatMaster.Date = "Master";
                        koordinatMaster.Longitude = warehouse.Longitude;
                        koordinatMaster.Latitude = warehouse.Latitude;

                        listKoordinat.Insert(0, koordinatMaster);

                        dgKunjungan1.DataSource = listKoordinat;
                        dgKunjungan1.DataBind();

                        if (listKoordinat.Count > 1)
                        {
                            rptMarkers.DataSource = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, lblNama.Text, warehouseCustomer);
                            rptMarkers.DataBind();
                        }
                        else
                        {
                            rptMarkers.DataSource = listKoordinat;
                            rptMarkers.DataBind();
                        }
                    }

                    radius.Value = Convert.ToString(warehouse.Radius);
                }
            }
            latbox.Value = "";
            lngbox.Value = "";
        }

        //protected void dgKunjungan1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        //{
        //    //dgKunjungan1.CurrentPageIndex = e.NewPageIndex;

        //    //bindRBLColor();

        //    string rbCustomer = lblNama.Text;
        //    string txtCustomerId = Session["txtCustomerId"].ToString();
        //    string cldF = Session["CldFrom"].ToString();
        //    string cldT = Session["CldTo"].ToString();
        //    DateTime cldFrom = Convert.ToDateTime(cldF);
        //    DateTime cldTo = Convert.ToDateTime(cldT);

        //    var lCustomerlist = CustomerFacade.LoadCustomernWarehouseAddr(rbCustomer, txtCustomerId);

        //    foreach (var lcustomer in lCustomerlist)
        //    {
        //        if (lcustomer.CustomerType == "customer")
        //        {
        //            var listCustomer = new List<Core.Model.Customer>();
        //            var customer = CustomerFacade.GetCustomerDetail(rbCustomer);
        //            if (customer == null)
        //            {

        //            }
        //            else
        //            {
        //                var listKoordinat = VisitFacade.GetKoordinatKunjungan(cldFrom, cldTo, rbCustomer);

        //                var koordinatMaster = new Core.Model.mdlKoordinatKunjungan();
        //                koordinatMaster.Date = "Master";
        //                koordinatMaster.Longitude = customer.Longitude;
        //                koordinatMaster.Latitude = customer.Latitude;

        //                listKoordinat.Insert(0, koordinatMaster);

        //                dgKunjungan1.DataSource = listKoordinat;
        //                dgKunjungan1.DataBind();
        //            }
        //        }
        //        else
        //        {
        //            var listWarehouse = new List<Core.Model.Warehouse>();
        //            var warehouse = WarehouseFacade.GetWarehouseDetail(rbCustomer, txtCustomerId);
        //            if (warehouse == null)
        //            {

        //            }
        //            else
        //            {
        //                string warehouseCustomer = warehouse.CustomerID;

        //                var listKoordinat = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, rbCustomer, warehouseCustomer);

        //                var koordinatMaster = new Core.Model.mdlKoordinatKunjunganGudang();
        //                koordinatMaster.Date = "Master";
        //                koordinatMaster.Longitude = warehouse.Longitude;
        //                koordinatMaster.Latitude = warehouse.Latitude;

        //                listKoordinat.Insert(0, koordinatMaster);

        //                dgKunjungan1.DataSource = listKoordinat;
        //                dgKunjungan1.DataBind();
        //            }
        //        }
        //    }
        //}

        //protected void dgToko_ItemCommand(object source, DataGridCommandEventArgs e)
        //{
        //    if (e.CommandName == "idtoko")
        //    {
        //        dgKunjungan1.CurrentPageIndex = 0;

        //        lblNama.Text = e.Item.Cells[1].Text;

        //        lblKeterangan.Text = e.Item.Cells[1].Text;
        //        lblAlamat.Text = e.Item.Cells[3].Text;

        //        string customerID = Session["txtCustomerId"].ToString();
        //        string cldF = Session["CldFrom"].ToString();
        //        string cldT = Session["CldTo"].ToString();
        //        DateTime cldFrom = Convert.ToDateTime(cldF);
        //        DateTime cldTo = Convert.ToDateTime(cldT);

        //        var lCustomerlist = CustomerFacade.LoadCustomernWarehouseAddr(lblNama.Text, customerID);
        //        foreach (var lcustomer in lCustomerlist)
        //        {
        //            lblAlamat.Text = lcustomer.CustomerAddress;

        //            if (lcustomer.CustomerType == "customer")
        //            {
        //                var listCustomer = new List<Core.Model.Customer>();
        //                var customer = CustomerFacade.GetCustomerDetail(lblNama.Text);
        //                if (customer == null)
        //                {

        //                }
        //                else
        //                {
        //                    listCustomer.Add(customer);

        //                    rptVisit.DataSource = listCustomer;
        //                    rptVisit.DataBind();

        //                    var listKoordinat = VisitFacade.GetKoordinatKunjungan(cldFrom, cldTo, lblNama.Text);

        //                    var koordinatMaster = new Core.Model.mdlKoordinatKunjungan();
        //                    koordinatMaster.Date = "Master";
        //                    koordinatMaster.Longitude = customer.Longitude;
        //                    koordinatMaster.Latitude = customer.Latitude;

        //                    listKoordinat.Insert(0, koordinatMaster);

        //                    dgKunjungan1.DataSource = listKoordinat;
        //                    dgKunjungan1.DataBind();

        //                    if (listKoordinat.Count > 1)
        //                    {
        //                        rptMarkers.DataSource = VisitFacade.GetKoordinatKunjungan(cldFrom, cldTo, lblNama.Text);
        //                        rptMarkers.DataBind();
        //                    }
        //                    else
        //                    {
        //                        rptMarkers.DataSource = listKoordinat;
        //                        rptMarkers.DataBind();
        //                    }
        //                }

        //                radius.Value = Convert.ToString(customer.Radius); //001
        //            }
        //            else
        //            {
        //                var listWarehouse = new List<Core.Model.Warehouse>();
        //                var warehouse = WarehouseFacade.GetWarehouseDetail(lblNama.Text, customerID);
        //                if (warehouse == null)
        //                {

        //                }
        //                else
        //                {
        //                    listWarehouse.Add(warehouse);

        //                    rptVisit.DataSource = listWarehouse;
        //                    rptVisit.DataBind();

        //                    string warehouseCustomer = warehouse.CustomerID;

        //                    var listKoordinat = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, lblNama.Text, warehouseCustomer);

        //                    var koordinatMaster = new Core.Model.mdlKoordinatKunjunganGudang();
        //                    koordinatMaster.Date = "Master";
        //                    koordinatMaster.Longitude = warehouse.Longitude;
        //                    koordinatMaster.Latitude = warehouse.Latitude;

        //                    listKoordinat.Insert(0, koordinatMaster);

        //                    dgKunjungan1.DataSource = listKoordinat;
        //                    dgKunjungan1.DataBind();

        //                    if (listKoordinat.Count > 1)
        //                    {
        //                        rptMarkers.DataSource = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, lblNama.Text, warehouseCustomer);
        //                        rptMarkers.DataBind();
        //                    }
        //                    else
        //                    {
        //                        rptMarkers.DataSource = listKoordinat;
        //                        rptMarkers.DataBind();
        //                    }
        //                }

        //                radius.Value = Convert.ToString(warehouse.Radius);
        //            }
        //        }
        //        latbox.Value = "";
        //        lngbox.Value = "";
        //    }
        //}

        //protected void dgToko_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        //{
        //    dgToko.CurrentPageIndex = e.NewPageIndex;
            
        //    string txtCustomerId = Session["txtCustomerId"].ToString();

        //    var lCustomerlist = CustomerFacade.LoadCustomernWarehouse(txtCustomerId);

        //    dgToko.DataSource = lCustomerlist;
        //    dgToko.DataBind();
        //}

    }
}