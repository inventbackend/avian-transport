﻿/* documentation
 *001 17 Okt 2016 fernandes
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;


public partial class AddCallPlanDetail : System.Web.UI.Page
{
    public string gBranchId, gUserId;

    private void ClearContents()
    {
        txtCustomerID.Text = string.Empty;
        txtSequence.Text = string.Empty;
        txtCallPlanID.Text = string.Empty;
        PanelCustomer.Visible = false;
        PanelWarehouse.Visible = false; //001
    }

    public string getBranch()
    {
        //get user branchid
        gUserId = Session["User"].ToString();
        var vUser = UserFacade.GetUserbyID(gUserId);
        gBranchId = vUser.BranchID;

        return gBranchId;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string status = Request.QueryString["cadd"];
            string id = Request.QueryString["cid"]; 

            if (status == "add")
            {
                

                txtCallPlanID.Text = id; 
                txtCallPlanID.Enabled = false; 

                btnUpdate.Visible = false;
            }

            else
            {
                txtCPDetailID.Text = Request.QueryString["cid"];

                var cpDetail = CallPlanFacade.GetCallPlanByCPDetailID(txtCPDetailID.Text);
                txtCallPlanID.Text = cpDetail.CallPlanID;
                txtCallPlanID.Enabled = false;

                txtCustomerID.Text = cpDetail.CustomerID;
                txtWarehouseID.Text = cpDetail.WarehouseID; //001
               

                string anString;
                anString = Convert.ToString(cpDetail.Sequence);

                txtSequence.Text = anString;
                

                btnInsert.Visible = false;

            }
        }

        //btnCancel.Attributes.Add("onClick", "javascript:history.back(); return false;"); //001
        PanelCustomer.Visible = false;
        PanelWarehouse.Visible = false; //001
       
    }

    //001
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        int anInteger;
        anInteger = Convert.ToInt32(txtSequence.Text);
        anInteger = int.Parse(txtSequence.Text);

        string id;
        id = txtCallPlanID.Text;

        var cpDetail = CallPlanFacade.GetCallPlanByCPDetailID(txtCPDetailID.Text);
        if (cpDetail.CustomerID != txtCustomerID.Text || cpDetail.WarehouseID != txtWarehouseID.Text)
        {
            var SameCustomer = CallPlanFacade.GetSameCustomer(txtCustomerID.Text, txtCallPlanID.Text, txtWarehouseID.Text);
            if (SameCustomer != null)
            {
                Response.Write("<script>alert('The Customer or Warehouse that you insert is already Exist in this Call Plan');</script>");
                return;
            }
        }

        CallPlanFacade.EditCPDetail(txtCPDetailID.Text, txtCallPlanID.Text, txtCustomerID.Text, anInteger, txtWarehouseID.Text);
        Response.Redirect("CallPlanDetail.aspx?cid=" + id);

        ClearContents();
    }

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        string id;
        id = txtCallPlanID.Text;

        int anInteger;
        anInteger = Convert.ToInt32(txtSequence.Text);
        anInteger = int.Parse(txtSequence.Text);

        var SameCustomer = CallPlanFacade.GetSameCustomer(txtCustomerID.Text, txtCallPlanID.Text, txtWarehouseID.Text); //001
        if (SameCustomer != null)
        {
            Response.Write("<script>alert('The Customer or Warehouse that you insert is already Exist in this Call Plan');</script>"); //001
            return;
        }

        CallPlanFacade.InsertCPDetail(txtCallPlanID.Text, txtCustomerID.Text, txtWarehouseID.Text, anInteger); //001
        Response.Redirect("CallPlanDetail.aspx?cid=" + id);

        ClearContents();
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string id;
        id = txtCallPlanID.Text;

        Response.Redirect("CallPlanDetail.aspx?cid=" + id);
    }

    protected void rptCustomer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Link")
        {
            Control control;
            control = e.Item.FindControl("lnkCustomerId");
            if (control != null)
            {
                string id;
                id = ((LinkButton)control).Text;
                txtCustomerID.Text = id;
                PanelCustomer.Visible = false;
            }
        }
    }

    private void BindDataCustomer()
    {
        if (getBranch() == "")
        {
            RptCustomerlist.DataSource = CustomerFacade.GetSearch(txtSearchCustomer.Text, getBranch());
            RptCustomerlist.DataBind();
        }
        else
        {
            RptCustomerlist.DataSource = CustomerFacade.GetSearchCustomerByBranch(txtSearchCustomer.Text, getBranch());
            RptCustomerlist.DataBind();
        }
    }

    protected void btnShowCustomer_Click(object sender, EventArgs e)
    {
        txtSearchCustomer.Text = string.Empty;
        BindDataCustomer();
        PanelCustomer.Visible = true;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindDataCustomer();
        PanelCustomer.Visible = true;
    }

    protected void btnCancelCustomer_Click(object sender, EventArgs e)
    {
        PanelCustomer.Visible = false;
        txtSearchCustomer.Text = string.Empty;
    }

    //--001
    private void BindDataWarehouse()
    {
        RptWarehouselist.DataSource = WarehouseFacade.GetSearch(txtCustomerID.Text, txtSearchWarehouse.Text);
        RptWarehouselist.DataBind();
    }

    protected void btnShowWarehouse_Click(object sender, EventArgs e)
    {
        txtSearchWarehouse.Text = "";
        BindDataWarehouse();
        PanelWarehouse.Visible = true;
    }

    protected void btnSearchWarehouse_Click(object sender, EventArgs e)
    {
        BindDataWarehouse();
        PanelWarehouse.Visible = true;
    }

    protected void btnCancelWarehouse_Click(object sender, EventArgs e)
    {
        PanelWarehouse.Visible = false;
        txtSearchWarehouse.Text = string.Empty;
    }

    protected void rptWarehouse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "LinkWarehouse")
        {
            Control control;
            control = e.Item.FindControl("lnkWarehouseId");
            if (control != null)
            {
                string id;
                id = ((LinkButton)control).Text;
                txtWarehouseID.Text = id;
                PanelWarehouse.Visible = false;
            }
        }
    }
    //001--

}
