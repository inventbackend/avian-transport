﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;

namespace TransportService.pages
{
    public partial class JourneyTracking : System.Web.UI.Page
    {
        private void ClearContent()
        {
            txtBranchID.Text = string.Empty;
            txtDate.Text = string.Empty;
            txtEmployee.Text = string.Empty;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ClearContent();
            }

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            var listJourneyTracking = new List<Core.Model.mdlJourneyTracking>();

            listJourneyTracking.AddRange(TrackingFacade.GetJourneyTracking(txtBranchID.Text, Convert.ToDateTime(txtDate.Text), txtEmployee.Text));
            if (listJourneyTracking.Count == 0)
            {
                lblAlert.Text = "Location unavailable";
            }
            else
            {
                lblAlert.Text = "";
                rptJourneyMarkers.DataSource = listJourneyTracking;
                rptJourneyMarkers.DataBind();
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBranchID(string prefixText, int count)
        {
            List<string> lBranchs = new List<string>();
            lBranchs = BranchFacade.AutoComplBranch(prefixText, count, "BranchID");
            return lBranchs;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchEmployee(string prefixText, int count)
        {
            List<string> lEmployees = new List<string>();
            lEmployees = EmployeeFacade.AutoComplEmployee(prefixText, count, "EmployeeID");
            return lEmployees;
        }

    }
}