﻿<%@ Page Title="VISIT TRACKING" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="Tracking.aspx.cs" Inherits="TransportService.pages.Tracking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Visit Tracking</h3>
        </div>
        <%--<div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>
        </div>--%>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Filter By</small></h2>
                <%--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>--%>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <br />
                <div class="form-group">
                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                    </asp:ScriptManager>
                    <p>
                    <asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Width="100px" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; margin-left:-10px"></asp:Label>
                    <asp:Label ID="Label2" runat="server" Text=":" Width="10px" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; margin-left:5px"></asp:Label>
                        <%--<asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Width="100px"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Text=":" Width="10px"></asp:Label>--%>

                        <%--<asp:TextBox ID="txtBranchID" runat="server" Width="150px" CssClass="tb5"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBranchID" ValidationGroup="valShowReport" ForeColor="#FF3300"
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <ajaxToolkit:AutoCompleteExtender ID="txtBranchID_AutoCompleteExtender" runat="server"
                            ServiceMethod="SearchBranchID" MinimumPrefixLength="2" EnableCaching="false"
                            DelimiterCharacters="" Enabled="True" TargetControlID="txtBranchID">
                        </ajaxToolkit:AutoCompleteExtender>--%>
                        <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                        </asp:DropDownList>
                    </p>
                    <p>
                        <asp:Label ID="lblEmployeeID" runat="server" Text="Employee ID" Width="100px"></asp:Label>
                        <asp:Label ID="Label4" runat="server" Text=":" Width="10px"></asp:Label>
                        <asp:TextBox ID="txtEmployeeID" runat="server" Width="150px" CssClass="tb5" PlaceHolder="Search Employee"></asp:TextBox>
                        <%--001 nanda--%>
                        <asp:Button ID="btnShowEmployee" runat="server" CssClass="btn btn-sm " Text="Show Employee"
                            OnClick="btnShowEmployee_Click" />
                        <%-- Validasi Checkbox--%>
                        <asp:Label ID="lblValblEmployee" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                        <asp:Panel ID="Panel1" CssClass="form-control" Height="200px" Width="400px" runat="server"
                            ScrollBars="Vertical">
                            <asp:CheckBoxList ID="blEmployee" runat="server">
                            </asp:CheckBoxList>
                        </asp:Panel>
                        <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-sm " Text="SELECT ALL"
                            OnClick="btnSelectAll_Click" />
                        <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-sm " Text="UNSELECT ALL"
                            OnClick="btnUnSelectAll_Click" />
                    </p>
                    <p>
                        <asp:Label ID="lblDODate" runat="server" Text="Date" Width="100px"></asp:Label>
                        <asp:Label ID="Label3" runat="server" Text=":" Width="10px"></asp:Label>
                        <asp:TextBox ID="txtDODate" runat="server" Width="150px" CssClass="tb5" PlaceHolder="Start Date"></asp:TextBox>
                        <script type="text/javascript">
                            var picker1 = new Pikaday(
                                                        {
                                                            field: document.getElementById('<%=txtDODate.ClientID%>'),
                                                            firstday: 1,
                                                            minDate: new Date('2000-01-01'),
                                                            maxDate: new Date('2020-12-31'),
                                                            yearRange: [2000, 2020]
                                                            //                                    setDefaultDate: true,
                                                            //                                    defaultDate : new Date()
                                                        }
                                                        );
                        </script>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtDODate" ValidationGroup="valShowReport" ForeColor="#FF3300"
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:Label ID="Label1" runat="server" Text="To" Width="50px" Style="text-align: center"></asp:Label>
                        <asp:TextBox ID="txtdate2" runat="server" Width="150px" CssClass="tb5" PlaceHolder="End Date"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtdate2" ValidationGroup="valShowReport" ForeColor="#FF3300"
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <script type="text/javascript">
                            var picker2 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtdate2.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                        </script>
                    </p>
                    <asp:Button ID="btnShow" runat="server" Text="Show Tracking" class="btn btn-sm btn-embossed btn-primary"
                        Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                </div>
            </div>
        </div>
    </div>
    <%-- SHOW MAP--%>
    <div class="col-md-12 col-xs-12">
                <asp:Label ID="lblAlert" runat="server" Text="" Style="text-align: center; color: #FF0000;
                    font-weight: 700"></asp:Label>
                <div id="dvMap" style="width: 100%; height: 640px">
          
           
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <link href="../js/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4a8ijWorZUkJKOtzheGKtV1RlyfDYW24&callback=initMap"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>--%>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry,places"></script>--%>
    <!-- Support Jquery -->
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script src="../js/MapDefault.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/GoogleMap.js"></script>
    <script src="../js/mapwithmarker.js" type="text/javascript"></script>
    <style type="text/css">
        .labels
        {
            color: black;
            background-color: #FF8075;
            font-family: Arial;
            font-size: 11px;
            font-weight: bold;
            text-align: center;
            width: 25px;
        }
    </style>
    <script type="text/javascript">
        var branchCoordinate = [
        <asp:Repeater ID="rptBranchCoordinate" runat="server">
        <ItemTemplate>
                    {
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>'
                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];
    </script>
    <script type="text/javascript">
        function loadMap() {

            //Create Map
            var objGoogleMapOption = {
                center: new google.maps.LatLng(branchCoordinate[0].lat, branchCoordinate[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            objMap = new google.maps.Map(document.getElementById("dvMap"), objGoogleMapOption);

        }

        $(function () {
            loadMap();
        });
    </script>
    <script type="text/javascript">
        var markers = [
        <asp:Repeater ID="rptVisitMarkers" runat="server">
        <ItemTemplate>
                    {
                    "title": '<%# Eval("Customer") %>',
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>',
                    "description": '<%# Eval("Customer") %>',
                    "Start": '<%# Eval("Start") %>',
                    "End": '<%# Eval("End") %>',
                    "Duration": '<%# Eval("Duration") %>',
                    "lStreetName": '<%# Eval("StreetName") %>',
                    "lEmployeeID": '<%# Eval("EmployeeID") %>',
                    "CustomerID" : '<%# Eval("CustomerID") %>'
                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];

        var Customermarkers = [
        <asp:Repeater ID="rptCustomerMarkers" runat="server">
        <ItemTemplate>
                    {
                    "title": '<%# Eval("CustomerName") %>',
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>',
                    "description": '<%# Eval("CustomerName") %>',
                    "address": '<%# Eval("CustomerAddress") %>',
                    "CustomerID" : '<%# Eval("CustomerID") %>'
                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];
    </script>
    <script type="text/javascript">

        //munculin titik kunjungan
        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var infowindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);


            for (i = 0; i < markers.length; i++) {
                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var contentString = '<div id="content">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<h1 id="firstHeading" class="firstHeading">' + data.description + ' - (' + data.CustomerID + ')' + '</h1>' +
                                '<div id="bodyContent">' +
                                '<p><b>Start    : </b>' + data.Start + '</p>' +
                                '<p><b>End : </b>' + data.End + '</p>' +
                                '<p><b>Duration : </b>' + data.Duration + '</p>' +
                                '<p><b>Street Name : </b>' + data.lStreetName + '</p>' +
                                '<p><b>Employee ID : </b>' + data.lEmployeeID + '</p>' +
                                '</div>' +
                                '</div>';

                var marker = new MarkerWithLabel({
                    position: myLatlng,
                    map: map,
                    title: data.title + " - (" + data.CustomerID + ")",
                    labelContent: i,
                    labelAnchor: new google.maps.Point(15, 65),
                    labelClass: "labels", // the CSS class for the label
                    labelInBackground: false,
                    icon: "../Images/Visit.png"
                });

                google.maps.event.addListener(marker, 'click', (function (marker, contentString, infowindow) {
                    return function () {
                        infowindow.setContent(contentString);
                        infowindow.open(map, marker);
                    };
                })(marker, contentString, infowindow));
            }

            //munculin titik toko
            for (j = 0; j < Customermarkers.length; j++) {
                var dataCustomer = Customermarkers[j];
                var myCustomerLatlng = new google.maps.LatLng(dataCustomer.lat, dataCustomer.lng);
                var contentCustomerString = '<div id="content">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<h1 id="firstHeading" class="firstHeading">' + dataCustomer.description + ' - (' + dataCustomer.CustomerID + ')' + '</h1>' +
                                '<div id="bodyContent">' +
                                '<p><b>Name    : </b>' + dataCustomer.description + '</p>' +
                                '<p><b>Address : </b>' + dataCustomer.address + '</p>' +
                                '</div>' +
                                '</div>';

                var markerCustomer = new MarkerWithLabel({
                    position: myCustomerLatlng,
                    map: map,
                    title: dataCustomer.title + " - (" + dataCustomer.CustomerID + ")",
                    labelContent: j,
                    labelAnchor: new google.maps.Point(15, 65),
                    labelClass: "labels", // the CSS class for the label
                    labelInBackground: false,
                    icon: "../Images/shop.png"
                });

                google.maps.event.addListener(markerCustomer, 'click', (function (markerCustomer, contentCustomerString, infowindow) {
                    return function () {
                        infowindow.setContent(contentCustomerString);
                        infowindow.open(map, markerCustomer);
                    };
                })(markerCustomer, contentCustomerString, infowindow));
            }
        }
    </script>
</asp:Content>
