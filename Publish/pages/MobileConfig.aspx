﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="MobileConfig.aspx.cs" Inherits="TransportService.pages.MobileConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Bootstrap Colorpicker -->
    <link href="../vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css"
        rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Mobile Config</h3>
        </div>
        <div class="title_right">
            <%--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>--%>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <br />
                <div class="form-group">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <div class="col-md-2">
                        <label class="control-label" style="margin-top: 5px">
                            Branch ID :</label>
                    </div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" Style="margin-left: -21px">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6">
                        <asp:Button ID="btnSearchBranch2" runat="server" Text="Search" class="btn btn-primary"
                            OnClick="btnSearchBranch2_Click" />
                        <asp:Button ID="btnNew" runat="server" Text="Add Config" class="btn btn-primary"
                            OnClick="btnNew_Click"></asp:Button>
                        <br />
                    </div>
                    <div class="datagrid" style="width: 100%">
                        <table style="width: 100%">
                            <thead>
                                <tr>
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Desc
                                    </th>
                                    <th>
                                        Value
                                    </th>
                                    <th>
                                        Branch ID
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="RptMobileConfiglist" runat="server" OnItemCommand="rptMobileConfig_ItemCommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkId" runat="server" Enabled='<%# Eval("Role") %>' CommandName="Link" Text='<%# Eval("ID") %>'></asp:LinkButton>
                                            </td>
                                            <td>
                                                <%# Eval("Desc") %>
                                            </td>
                                            <td>
                                                <%# Eval("Value") %>
                                            </td>
                                            <td>
                                                <%# Eval("BranchId")%>
                                            </td>
                                           <td>
                                                        <asp:LinkButton ID="lnkDelete" Text="Delete" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Are you sure you want to Delete this Mobile Config?');"
                                                            runat="server" CommandName="Delete" />
                                        </tr>
                                        </tbody>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <div id="paging">
                                            <ul>
                                                <li>
                                                    <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                    <ItemTemplate>
                                                        <li>
                                                            <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <li>
                                                    <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="PanelFormMobileConfig" runat="server" Width="100%">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Form Mobile Config Management
                    </h2>
                    <%--<ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a> </li>
                                <li><a href="#">Settings 2</a> </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>--%>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <asp:Label ID="lblID" runat="server" Text="ID" Font-Bold="True"></asp:Label><br />
                            <asp:TextBox ID="txtID" PlaceHolder="ID" runat="server" Width="400px" class="form-control"
                                ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                ControlToValidate="txtID" ValidationGroup="valBranch" ForeColor="#FF3300" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <asp:Label ID="lblValue" runat="server" Text="Value" Font-Bold="True"></asp:Label><br />
                            <asp:DropDownList ID="ddlValue" runat="server" Width="400px" class="form-control">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtValue" runat="server" Width="400px" class="form-control"></asp:TextBox>
                            <asp:TextBox ID="txtValueColor" runat="server" Width="400px" class="demo1 form-control"></asp:TextBox>
                            <asp:TextBox ID="txtValueInt" runat="server" Width="400px" class="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtValueInt"
                                ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$"
                                ValidationGroup="valMobileConfig">
                            </asp:RegularExpressionValidator>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <asp:Label ID="lblDesc" runat="server" Text="Description" Font-Bold="True"></asp:Label><br />
                            <asp:TextBox ID="txtDesc" PlaceHolder="Description" runat="server" Width="400px" Height ="32px"
                                class="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <br />
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:Label ID="Label1" runat="server" Text="Type Value" Font-Bold="True"></asp:Label><br />
                        <asp:DropDownList ID="ddlTypeValue" runat="server" Width="400px"  
                            class="form-control" 
                            onselectedindexchanged="ddlTypeValue_SelectedIndexChanged" 
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <br />
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click"
                                ValidationGroup="valMobileConfig" />
                            <asp:Button ID="btnInsert" runat="server" Text="Insert" class="btn btn-primary" OnClick="btnInsert_Click"
                                ValidationGroup="valMobileConfig" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-primary" OnClick="btnCancel_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="../vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"
        type="text/javascript"></script>
    <!-- Bootstrap Colorpicker -->
    <script type="text/javascript">
      $(document).ready(function() {
        $('.demo1').colorpicker();
        $('.demo2').colorpicker();

        $('#demo_forceformat').colorpicker({
            format: 'rgba',
            horizontal: true
        });

        $('#demo_forceformat3').colorpicker({
            format: 'rgba',
        });

        $('.demo-auto').colorpicker();
      });
    </script>
    <!-- /Bootstrap Colorpicker -->
</asp:Content>
