﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="CallPlan.aspx.cs" Inherits="TransportService.pages.CallPlan" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form CallPlan</h3>
        </div>
        <div class="title_right">
            <%--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>--%>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <%--<div class="x_title">
                <h2>
                    Form Basic Elements <small>different form elements</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>--%>
            <div class="x_content">
                <br />
                <div class="datagrid" style="width: 1000px">
                <%--<div class="datagrid" style="width: 100%">--%>
                    <table>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <br />
                                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                                    </asp:ScriptManager>
                                    <p>
                                        <asp:Label ID="lblEmployeeID" runat="server" Text="Employee ID" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                                        <asp:TextBox ID="txtEmployeeID" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                            Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Employee ID"></asp:TextBox>
                                        <%--<ajaxtoolkit:autocompleteextender id="txtEmployeeID_AutoCompleteExtender" runat="server"
                                            servicemethod="SearchEmployeeID" minimumprefixlength="2" enablecaching="false"
                                            delimitercharacters="" enabled="True" targetcontrolid="txtEmployeeID"> 
                                                    </ajaxtoolkit:autocompleteextender>--%>
                                        <asp:Button ID="btnShowEmployee" runat="server" Text="Show All Employee" Style="margin-left: 10px;
                                            margin-top: 0px" class="btn btn-primary" OnClick="btnShowEmployee_Click"></asp:Button>

                                        <asp:TextBox ID="txtSearchEmployee" runat="server" Style="margin-left: 0px; margin-top: 0px"
                                            Width="200px" CssClass="tb5" PlaceHolder="By Employee Id or Name"></asp:TextBox>
                                        <ajaxtoolkit:autocompleteextender id="txtEmployeeName_AutoCompleteExtender" runat="server"
                                            servicemethod="SearchEmployeeName" minimumprefixlength="2" enablecaching="false"
                                            delimitercharacters="" enabled="false" targetcontrolid="txtSearchEmployee"> 
                                                    </ajaxtoolkit:autocompleteextender>

                                        <asp:Button ID="btnSearch" runat="server" Text="Search Employee" Style="margin-left: 0px;
                                            margin-top: 0px" OnClick="btnSearch_Click" class="btn btn-primary"></asp:Button>
                                        <asp:Button ID="btnCancelEmployee" runat="server" Text="Cancel" Style="margin-left: 0px;
                                            margin-top: 0px" class="btn btn-primary" OnClick="btnCancelEmployee_Click"></asp:Button>

                                    </p>
                                    <asp:Panel ID="PanelEmployee" runat="server" Width="100%" Height="80px" ScrollBars="Vertical"
                                        CssClass="datagrid">
                                        <table style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Employee ID
                                                    </th>
                                                    <th>
                                                        Employee Name
                                                    </th>
                                                    <th>
                                                        EmployeeType ID
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="RptEmployeelist" runat="server" OnItemCommand="rptEmployee_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton ID="lnkEmployeeId" runat="server" CommandName="Link" Text='<%# Eval("EmployeeID") %>'></asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <%# Eval("EmployeeName")%>
                                                            </td>
                                                            <td>
                                                                <%# Eval("EmployeeTypeID")%>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                    <asp:Label ID="lblDate" runat="server" Text="Date" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                                    <asp:Label ID="Label1" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                                    <asp:TextBox ID="txtDate" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                        Width="150px" CssClass="form-control col-md-3 col-xs-12" placeholder="*Required..."></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="Required1" runat="server" ErrorMessage="*" ControlToValidate="txtDate"
                                        ValidationGroup="callplan" ForeColor="#FF3300" SetFocusOnError="True">
                                    </asp:RequiredFieldValidator>
                                    <script type="text/javascript">
                                        var picker1 = new Pikaday(
                                {
                                    field: document.getElementById('<%=txtDate.ClientID%>'),
                                    firstday: 1,
                                    minDate: new Date('2000-01-01'),
                                    maxDate: new Date('2020-12-31'),
                                    yearRange: [2000, 2020]
                                    //                                    setDefaultDate: true,
                                    //                                    defaultDate : new Date()
                                }
                                );
                                    </script>
                                    <asp:Button ID="btnSearch2" runat="server" Text="Search" OnClick="btnSearch2_Click"
                                        class="btn btn-primary" ValidationGroup="callplan"></asp:Button>
                                    <%-- <asp:Button ID="btnShowAll" runat="server" Text="Show All" class="btn btn-primary" OnClick="btnSA_Click" />--%>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="datagrid" style="width: 100%">
                        <table style="width: 100%">
                            <thead>
                                <tr>
                                    <th>
                                        Call Plan ID
                                    </th>
                                    <th>
                                        Employee ID
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Vehicle Number
                                    </th>
                                    <th>
                                        Branch ID
                                    </th>
                                    <%--<th>
                                                    </th>--%>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="RptCallPlanlist" runat="server" OnItemCommand="rptCallPlan_ItemCommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkCallPlanId" runat="server" Text='<%# Eval("CallPlanID") %>'
                                                    CommandName="Link"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <%# Eval("EmployeeID") %>
                                            </td>
                                            <td>
                                                <%# Eval("Date") %>
                                            </td>
                                            <td>
                                                <%# Eval("VehicleID") %>
                                            </td>
                                            <td>
                                                <%# Eval("BranchID") %>
                                            </td>
                                            <%--<td>
                                                                <asp:LinkButton ID="lnkDelete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this Product?');"
                                                                    runat="server" CommandName="Delete" />
                                                            </td>--%>
                                        </tr>
                                        </tbody>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            <div id="paging">
                                                <ul>
                                                    <li>
                                                        <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                    runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <li>
                                                        <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
