﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SettingKoordinatPop.aspx.cs" Inherits="TransportService.pages.SettingKoordinatPop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title></title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="../css/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="../css/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="../css/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="../css/bootstrap3-wysihtml5.min.css" rel="stylesheet"
        type="text/css" />
    <!-- Theme style -->
    <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />

    <link href="../js/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4a8ijWorZUkJKOtzheGKtV1RlyfDYW24&callback=initMap"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>--%>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry,places"></script>--%>
    <!-- Support Jquery -->
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script src="../js/MapDefault.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/GoogleMap.js"></script>
    <script src="../js/mapwithmarker.js" type="text/javascript"></script>
    <style type="text/css">
        .labels
        {
            color: black;
            background-color: #FF8075;
            font-family: Arial;
            font-size: 11px;
            font-weight: bold;
            text-align: center;
            width: 25px;
        }
    </style>

    <script type="text/javascript">

        function loadMap() {

            //Create Map
            objMap = new google.maps.Map(document.getElementById("map"), objGoogleMapOption);

        }

        $(function () {
            loadMap();
        });

        function loadMap2() {

            //Create Map
            objMap = new google.maps.Map(document.getElementById("map2"), objGoogleMapOption);

        }

        $(function () {
            loadMap2();
        });

    </script>

    <script type="text/javascript">
var markers = [
<asp:Repeater ID="rptMarkers" runat="server">
<ItemTemplate>
           {
            "title": '<%# Eval("Date") %>',
            "lat": '<%# Eval("Latitude") %>',
            "lng": '<%# Eval("Longitude") %>',
            "description": '<%# Eval("Date") %>'
        }
</ItemTemplate>
<SeparatorTemplate>
    ,
</SeparatorTemplate>
</asp:Repeater>
];

//var visit unused attribute
//            "title": '<%# Eval("CustomerName") %>',
//            "description": '<%# Eval("CustomerName") %>'

var visit = [
<asp:Repeater ID="rptVisit" runat="server">
<ItemTemplate>
           
         {
            "lat": '<%# Eval("Latitude") %>',
            "lng": '<%# Eval("Longitude") %>',
            "rad": <%# Eval("Radius") %>
        }
</ItemTemplate>
<SeparatorTemplate>
    ,
</SeparatorTemplate>
</asp:Repeater>
];
    </script>
    
    <script type="text/javascript">
        window.onload = function () {
            //set the zoom for map1
            if (document.getElementById('zoom_data_map').value == null || document.getElementById('zoom_data_map').value == '') {
                var nZoomMap1 = 10;
            }
            else {
                var zoomMap1 = document.getElementById('zoom_data_map').value;
                var nZoomMap1 = Number(zoomMap1);
            }
            //end of set the zoom for map1

            //get the last map position for the right side map before postback
            var latmap = markers[0].lat;
            var lngmap = markers[0].lng;

            if(document.getElementById('templatbox').value == null || document.getElementById('templatbox').value == '') {
            }
            else {
                latmap = document.getElementById('templatbox').value;
                lngmap = document.getElementById('templngbox').value;
            }

            var mapOptions = {
                center: new google.maps.LatLng(latmap, lngmap),
                zoom: nZoomMap1,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var path = new google.maps.MVCArray();
            var service = new google.maps.DirectionsService();
            var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
            var infoWindow = new google.maps.InfoWindow();

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            for (i = 0; i < markers.length; i++) {

                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);

                var dataMaster = visit[0];
                var masterLatlng = new google.maps.LatLng(dataMaster.lat, dataMaster.lng);

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title,
                    icon: "img/shop-red.png"
                });

                var gmarkers2 = []; //tempmastermarker for map in the right side

                var master = new google.maps.Marker({
                    position: new google.maps.LatLng(dataMaster.lat, dataMaster.lng),
                    map: map,
                    //                    title: dataMaster.title,
                    icon: "img/shop-green.png"
                });
                gmarkers2.push(master);

                var citycircle;
                if (visit.length > 0) {
                    citycircle = new google.maps.Circle({
                        center: masterLatlng,
                        strokecolor: '#ff0000',
                        strokeopacity: 0.8,
                        strokeweight: 2,
                        fillcolor: '#ff0000',
                        fillopacity: 0.35,
                        map: map,
                        radius: dataMaster.rad
                    });
                }

                var gmarkers = []; //tempmastermarker for map in the left side

                function addtempmasterMarker(tempmasterlocation) {
                    var tempmasterMarker = new google.maps.Marker({
                        draggable: true,
                        position: tempmasterlocation,
                        map: map2,
                        icon: "img/shop-green.png"
                    });

                    google.maps.event.addListener(tempmasterMarker, 'dragend', function (event) {
                        document.getElementById("latbox").value = this.getPosition().lat();
                        document.getElementById("lngbox").value = this.getPosition().lng();

                    });
                    gmarkers.push(tempmasterMarker);
                }

                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);

                        document.getElementById('latbox').value = data.lat;
                        document.getElementById('lngbox').value = data.lng;

                        var master = new google.maps.Marker({
                            position: new google.maps.LatLng(data.lat, data.lng),
                            map: map,
                            //                    title: dataMaster.title,
                            icon: "img/shop-green.png"
                        });
                        gmarkers2.push(master);
                        gmarkers2[0].setMap(null);
                        gmarkers2.splice(0, 1);

                        var tempmasterlocation = new google.maps.LatLng(data.lat, data.lng);
                        addtempmasterMarker(tempmasterlocation);

                        gmarkers[0].setMap(null);
                        gmarkers.splice(0, 1);

                        //lat and lng is available in e object
                        document.getElementById('templatbox').value = e.latLng.lat();
                        document.getElementById('templngbox').value = e.latLng.lng();
                    });
                })(marker, data);

                //get the last zoom position for map in the right side
                google.maps.event.addListener(map, 'zoom_changed', function () {
                    document.getElementById('zoom_data_map').value = map.getZoom();
                });

                //get the last latlong from the map in the right side
                google.maps.event.addListener(map, "click", function (e) {

                    //lat and lng is available in e object
                    document.getElementById('templatbox').value = e.latLng.lat();
                    document.getElementById('templngbox').value = e.latLng.lng();
                });
            }

            //set the zoom for map2
            if (document.getElementById('zoom_data_map2').value == null || document.getElementById('zoom_data_map2').value == '') {
                var nZoomMap2 = 12;
            }
            else {
                var zoomMap2 = document.getElementById('zoom_data_map2').value;
                var nZoomMap2 = Number(zoomMap2);
            }
            //end of //set the zoom for map2

            //get the last map position for the left side map before postback
            var latmap2 = visit[0].lat;
            var lngmap2 = visit[0].lng;

            if(document.getElementById('templatbox2').value == null || document.getElementById('templatbox2').value == '') {
            }
            else {
                latmap2 = document.getElementById('templatbox2').value;
                lngmap2 = document.getElementById('templngbox2').value;
            }

            var mapOptions2 = {
                center: new google.maps.LatLng(latmap2, lngmap2),
                zoom: nZoomMap2,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map2 = new google.maps.Map(document.getElementById("map2"), mapOptions2);

            for (j = 0; j < visit.length; j++) {
                var data2 = visit[j];
                var myLatlng2 = new google.maps.LatLng(data2.lat, data2.lng);
                var marker2 = new google.maps.Marker({
                    draggable: true,
                    position: myLatlng2,
                    map: map2,
                    //                    title: data2.title,
                    icon: "img/shop-green.png"
                });

                google.maps.event.addListener(marker2, 'dragend', function (event) {
                    document.getElementById("latbox").value = this.getPosition().lat();
                    document.getElementById("lngbox").value = this.getPosition().lng();

                });
                gmarkers.push(marker2);
                //                (function (marker2, data2) {
                //                    google.maps.event.addListener(marker2, "click", function (e) {
                //                        infoWindow.setContent(data2.description);
                //                        infoWindow.open(map2, marker2);
                //                    });
                //                })(marker2, data2);
            }

            //get the last zoom position for map in the left side
            google.maps.event.addListener(map2, 'zoom_changed', function () {
                document.getElementById('zoom_data_map2').value = map2.getZoom();
            });

            google.maps.event.addListener(marker2, "click", function (e) {
                //lat and lng is available in e object
                document.getElementById('templatbox2').value = e.latLng.lat();
                document.getElementById('templngbox2').value = e.latLng.lng();
            });

            //get the last latlong from the map in the left side
            google.maps.event.addListener(map2, "click", function (e) {

                //lat and lng is available in e object
                document.getElementById('templatbox2').value = e.latLng.lat();
                document.getElementById('templngbox2').value = e.latLng.lng();
            });

            //            var lat_lng = new Array();
            //            for (j = 0; j < tracking.length; j++) {
            //                var dataSrc = tracking[j];
            //                var start = new google.maps.LatLng(dataSrc.lat, dataSrc.lng);
            //                // var map = new google.maps.Map(document.getElementById("map"), myOptions);
            //                lat_lng.push(start);
            //                var marker1 = new google.maps.Marker({
            //                    position: start,
            //                    map: map,
            //                    title: dataSrc.title
            //                });
            //                (function (marker1, dataSrc) {
            //                    google.maps.event.addListener(marker1, "click", function (e) {
            //                        infoWindow.setContent(dataSrc.description);
            //                        infoWindow.open(map, marker1);
            //                    });
            //                })(marker1, dataSrc);
            //                //                (function (marker1, dataSrc) {

            //                //                }
            //            }

        }
    </script>
   
</head>
<body>
<div id="container" style="margin-top:100px; margin-left:150px;">

<form id="form1" runat="server">

<p style="font-weight:bold">
Toko/Gudang :
<asp:Panel ID="Panel1" CssClass="form-control" Height="160px" Width="300px" runat="server" ScrollBars="Vertical" style="margin-left:0px">
 <asp:RadioButtonList ID="rblCustomer" runat="server" 
        Style="margin-left:0px; width:300px" 
        onselectedindexchanged="rblCustomer_SelectedIndexChanged" AutoPostBack=true>
 </asp:RadioButtonList>
 <%--<asp:DataGrid ID="dgToko" class="table table-bordered" runat="server" 
                         AutoGenerateColumns="false" AllowPaging="True" PageSize="5"
                         onitemcommand="dgToko_ItemCommand" 
            onpageindexchanged="dgToko_PageIndexChanged"
                         >
                         
                         
                    <Columns >
                                        <asp:ButtonColumn DataTextField="CustomerID" HeaderText="Kode" CommandName="idtoko">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:ButtonColumn>
                                        <asp:BoundColumn DataField="CustomerID" HeaderText="Kode" Visible=false>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CustomerName" HeaderText="Nama">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CustomerAddress" HeaderText="Alamat">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                       
                                    </Columns>
                                     <PagerStyle CssClass="table_page" PrevPageText="Previous ‹.." NextPageText="..› Next" BackColor="#F7F7DE" ForeColor="Gray" HorizontalAlign="Left" Font-Bold="True" Font-Italic="False" Font-Names="verdana" Font-Overline="False" Font-Size="8pt" Font-Strikeout="False" Font-Underline="False" />
                    </asp:DataGrid>--%>
 </asp:Panel>

<p style="visibility:hidden">
 Kode Toko/Gudang : 
    <asp:label ID="lblNama" runat="server" Text="test" >
    </asp:label>
    </p>
 
 <p style="font-weight:bold">
 <%--KODE :
    <asp:label ID="lblKeterangan" runat="server" Text="test">
    </asp:label>
    <br />--%>
 ALAMAT : 
    <asp:label ID="lblAlamat" runat="server" Text="test">
    </asp:label>
    </p>
    
            <table cellspacing="2">
            <tr>
            <%--customer map--%>
            <td id="tdmap2" runat="server">
    <div id="map2" style="width:400px; height:400px"></div>
   
    </td>
    <td id="tdcmap" runat="server">&nbsp;&nbsp;</td>
    <td id="tdmap" runat="server">
              
        <div id="map" style="width:400px; height:400px"></div>
   
    </td>

    <td valign="top">
    <asp:Panel ID="Panel2" CssClass="form-control" Height="360px" Width="380px" runat="server" ScrollBars="Vertical" style="margin-left:10px">
    <asp:DataGrid ID="dgKunjungan1" class="table table-bordered" runat="server" 
                         AutoGenerateColumns="false" AllowPaging="False" PageSize="8" 
                         onitemcommand="dgKunjungan_ItemCommand">
                        <%-- onpageindexchanged="dgKunjungan1_PageIndexChanged"--%>
                         
                    <Columns >
                                        <asp:ButtonColumn DataTextField="Date" HeaderText="Tanggal" CommandName="id">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:ButtonColumn>
                                        <asp:BoundColumn DataField="Date" HeaderText="Tanggal" Visible=false>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Longitude" HeaderText="Longitude">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Latitude" HeaderText="Latitude">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                       
                                    </Columns>
                                     <PagerStyle CssClass="table_page" PrevPageText="Previous ‹.." NextPageText="..› Next" BackColor="#F7F7DE" ForeColor="Gray" HorizontalAlign="Left" Font-Bold="True" Font-Italic="False" Font-Names="verdana" Font-Overline="False" Font-Size="8pt" Font-Strikeout="False" Font-Underline="False" />
                    </asp:DataGrid>
    </asp:Panel>
    </td>
    </tr>
    <tr>
    <td></td>
    <td></td>
    <td></td>
    <td valign="top"> 
    <br />
    <p>Latitude: <input size="20" type="text" id="latbox" name="lat" runat="server" style="margin-left:10px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="latbox" ValidationGroup="valSettingKoordinatPop" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                     </p>
    <p>Longitude: <input size="20" type="text" id="lngbox" name="lng" runat="server" >
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="lngbox" ValidationGroup="valSettingKoordinatPop" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
    </p>
    <p>Radius: <input size="20" type="text" id="radius" name="rad" runat="server" style="margin-left:20px" placeholder="Fill with number...">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="radius" ValidationGroup="valSettingKoordinatPop" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
    </p></td>
            </tr>
           <tr>
            <td></td>
    <td></td>
    <td></td>
           <td> <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Save" ValidationGroup="valSettingKoordinatPop"/></td>
           </tr>
            </table>
<asp:TextBox ID="TextBox3" runat="server" style="visibility:hidden;"></asp:TextBox>

<p style="visibility:hidden">
 <%--get the last zoom position for the map in the left side--%>
 <input type="text" id="zoom_data_map2" runat="server" />

 <%--get the last zoom position for the map in the right side--%>
 <input type="text" id="zoom_data_map" runat="server" />

 <%--get the last current lat position for the map in the right side--%>
 <input type="text" id="templatbox" runat="server" />
 <%--get the last current lng position for the map in the right side--%>
 <input type="text" id="templngbox" runat="server" />

 <%--get the last current lat position for the map in the left side--%>
 <input type="text" id="templatbox2" runat="server" />
 <%--get the last current lng position for the map in the left side--%>
 <input type="text" id="templngbox2" runat="server" />
</p>

</form>
</div>
</body>
</html>
